$(function(){ // on dom ready

  $('#cy').cytoscape({

    style: cytoscape.stylesheet()
    .selector('node')
    .css({
      'width':'data(weight)',
      'height':'data(weight)',
      'background-fit': 'cover',
      'text-valign': 'bottom',
      'font-size': 9,
      'shape':'rectangle',
      'opacity':0.01,
      'background-image': 'data(image)',
    }),

    //hideLabelsOnViewport: true,
    elements: {
      nodes: demoNodes,
      edges: demoEdges
    },

    layout: {
      name: 'grid',
      //padding: 30, // padding used on fit
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
      rows: 15, // force num of rows in the grid
      columns: 15, // force num of cols in the grid  
      sort: function(node){ return node.data('weight')*100 ;}, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
      animate: false, // whether to transition the node positions
      animationDuration: 500, // duration of animation in ms if enabled
      position: function(node){ return {row:node.data('row'), col:node.data('col')};},
    },
    //autolock: true,

    // on graph initial layout done (could be async depending on layout...)
    ready: function(){
      window.cy = this;
      //cy.userZoomingEnabled( false );
      // cy.fit();
      // giddy up...
      // MENU
      cy.cxtmenu({
			selector: 'node',
			commands: [
				/*{
					content: '<span class="fa fa-venus-mars fa-x">Gender</span>',
					select: function(){
						cy.startBatch();
						var res;
						var node = cy.$('#'+this.id());
						var image = node.data('image');
						if (image.indexOf("female") > -1) {
							res = image.replace("female", "male");
							cy.$('#'+this.id()).data('gender', 'male');
							cy.$('#'+this.id()).data('image', res);
						} else if (image.indexOf("male") > -1) {
							res = image.replace("male", "female");
							cy.$('#'+this.id()).data('gender', 'female');
							cy.$('#'+this.id()).data('image', res);
						}
						cy.endBatch();
					}
				},
				{
					content: '<span class="fa fa-heartbeat fa-x">Alive</span>',
					select: function(){
						cy.startBatch();
						var res;
						var node = cy.$('#'+this.id());
						var image = node.data('image');
						if (image.indexOf("live") > -1) {
							res = image.replace("live", "dead");
							cy.$('#'+this.id()).data('status', 'dead');
							cy.$('#'+this.id()).data('image', res);
							
						} else if (image.indexOf("dead") > -1) {
							res = image.replace("dead", "live"); 
							cy.$('#'+this.id()).data('status', 'live');
							cy.$('#'+this.id()).data('image', res);
						}

						cy.endBatch();
					}
				//disabled: true
				},*/
				{
					content: '<span class="fa fa-pencil-square-o">Edit</span>',
					select: function(){
						document.getElementById('myNode').value = '#'+this.id();
						cy.startBatch();
				        var res;
						var node = cy.$('#'+this.id());
						var name = node.data('name');
						var gender = node.data('gender');
						var birthdate = node.data('birthDate');
						var status = node.data('status');
						var diseases = node.data('diseases');
						var image = node.data('image');
						var affection1 = node.data('affection1');
						var affection2 = node.data('affection2');
						var affection3 = node.data('affection3');
						var affection4 = node.data('affection4');
						//alert("NACE: "+birthdate);
						$('#cpButton1').colorpicker({showOn:'button'});
						$('#cpButton2').colorpicker({showOn:'button'});
						$('#cpButton3').colorpicker({showOn:'button'});
						$('#cpButton4').colorpicker({showOn:'button'});
						document.getElementById('myNode').value = '#'+this.id();
						document.getElementById('txname').value = name;
						document.getElementById('txdate').value = birthdate;
						$("#"+gender).prop("checked", true);
						$("#"+status).prop("checked", true);
						document.getElementById('affection1').value = affection1;
						document.getElementById('affection2').value = affection2;
						document.getElementById('affection3').value = affection3;
						document.getElementById('affection4').value = affection4;
						
						$("#editInformationModal").modal({backdrop: "static"});
						
						cy.endBatch();
					},
				},
				{
					content: '<span class="fa fa-star fa-x">Couple</span>',
					select: function(){
					
						//cy.startBatch();
						var node = cy.$('#'+this.id());
						var gender = node.data('gender');
						var row = node.data('row');
						var col = node.data('col');
						var position_x = node.position('x');
						var position_y = node.position('y');
						var coupleGender = "unknown";
						if (gender=="male") {
							coupleGender = "female";
						} else if (gender=="female") {
							coupleGender = "male";
						}
						$("#"+coupleGender+"1").prop("checked", true);
						$('#cpButton11').colorpicker({showOn:'button'});
						$('#cpButton21').colorpicker({showOn:'button'});
						$('#cpButton31').colorpicker({showOn:'button'});
						$('#cpButton41').colorpicker({showOn:'button'});
						if (!hasMarried(node)) {
							if (hasSpace(col+1, row, col+2, row)) {
								var new_id = col+2+"_"+row;
								document.getElementById('myNode').value = '#'+new_id;
								
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id:col+1+"_"+row,
															col: col+1,
															row: row, 
															weight:0, 
															image:"svgs/pixel.svg"
															},
									                 position: {x: position_x, y: position_y}},
							                       { group: "edges", 
							                    	 data: { source: col+1+"_"+row, target: node.id() } }
							                       ]);
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id: col+2+"_"+row,
															col: col+2,
															row: row, 
															weight:30, 
															name:"", 
															birthDate:"",
															gender:"unknown", 
															status:"live", 
															diseases:"0",
															content:"",  
															affection1:"", 
															affection2:"", 
															affection3:"", 
															affection4:"",
															image:"http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?gender=unknown&status=live&diseases=0"
															},
											          position: {x: position_x, y: position_y}}, 
							                       { group: "edges", 
							                    	 data: {source: col+2+"_"+row, target: col+1+"_"+row } }
							                       ]);
							    //cy.style().selector('node').style({}).update();
							    
							    var options = {
							    		name: 'grid',
							    		rows: 15, // force num of rows in the grid
							    		columns: 15, // force num of cols in the grid 
							    		animate: true, // whether to transition the node positions
							    	    animationDuration: 2000, // duration of animation in ms if enabled
							    	    autolock: true,
							    		//sort: function(node){ return node.data('weight')*100 ;}, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }    
							    		position: function(node){ return {row:node.data('row'), col:node.data('col')};},
							    };						
							    //cy.layout().run();
								$("#newPatientModal").modal({backdrop: "static"});
								var layout = cy.makeLayout(options);
								//cy.animate({fit:{}}, {duration: 800});
							    layout.run();
							    cy.elements().lock();
							    //cy.style().selector('node').style({}).update();
							    //cy.fit();
								//cleanModal();
      						} else if (hasSpace(col-1, row, col-2, row)) {
      							var new_id = col-2+"_"+row;
								document.getElementById('myNode').value = '#'+new_id;
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id:col-1+"_"+row,
															col: col-1,
															row: row, 
															weight:0, 
															image:"svgs/pixel.svg"
															},
											         position: {x: position_x, y: position_y}},
							                       { group: "edges", 
							                    	 data: { source: col-1+"_"+row, target: node.id() } }
							                       ]);
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id: col-2+"_"+row,
															col: col-2,
															row: row, 
															weight:30, 
															name:"", 
															birthDate:"",
															gender:"unknown", 
															status:"live", 
															diseases:"0",
															content:"",  
															affection1:"", 
															affection2:"", 
															affection3:"", 
															affection4:"",
															image:"http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?gender=unknown&status=live&diseases=0"
															},
											         position: {x: position_x, y: position_y}},
							                       { group: "edges", 
							                    	 data: {source: col-2+"_"+row, target: col-1+"_"+row } }
							                       ]);
							    var options = {
							    		name: 'grid',
							    		rows: 15, // force num of rows in the grid
							    		columns: 15, // force num of cols in the grid 
							    		animate: true, // whether to transition the node positions
							    	    animationDuration: 2000, // duration of animation in ms if enabled
							    	    autolock: true,
							    		//sort: function(node){ return node.data('weight')*100 ;}, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }    
							    		position: function(node){ return {row:node.data('row'), col:node.data('col')};},
							    };						
							    //cy.layout().run();
								$("#newPatientModal").modal({backdrop: "static"});
								var layout = cy.makeLayout(options);
								//cy.animate({fit:{}}, {duration: 800});
							    layout.run();
							    cy.elements().lock();
							    
							    //cy.style().selector('node').style({}).update();
							    //cy.layout.run();
							    //cy.animate({fit:{}}, {duration: 800});
							    
								//$("#newPatientModal").modal({backdrop: "static"});
								//cleanModal();
      						} else {
      							alert("There is no space for adding more nodes");
      						}
							
						} else {
							alert ("Patient is married (implemented soon)");
						}
						
						cy.elements().lock();
					},
					//disabled: true
				},
				{
					content: '<span class="fa fa-sitemap fa-x">Son</span>',
					select: function(){
						//cy.startBatch();
						var node = cy.$('#'+this.id());
						//var row = node.data('row');
						//var col = node.data('col');
						//var position_x = node.position('x');
						//var position_y = node.position('y');
						
						$('#cpButton11').colorpicker({showOn:'button'});
						$('#cpButton21').colorpicker({showOn:'button'});
						$('#cpButton31').colorpicker({showOn:'button'});
						$('#cpButton41').colorpicker({showOn:'button'});
						if (hasMarried(node)) { // Esta casado
							if (hasSons(node)) { // Tiene hijos
								marriedNode = getMarriedNode(node);
								gestationNode = getGestationNode(marriedNode);
								var row = gestationNode.data('row');
								var col = gestationNode.data('col');
								var position_x = gestationNode.position('x');
								var position_y = gestationNode.position('y');
								colNewSon = getSpaceSon(gestationNode);
																
								var new_id = colNewSon+"_"+row+1;
								document.getElementById('myNode').value = '#'+new_id;
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id:colNewSon+"_"+row,
															col: colNewSon,
															row: row, 
															weight:0, 
															image:"svgs/pixel.svg"
															},
									                 position: {x: position_x, y: position_y}},
							                       { group: "edges", 
							                    	 data: { source: colNewSon+"_"+row, target: gestationNode.id() } }
							                       ]);
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id: colNewSon+"_"+row+1,
															col: colNewSon,
															row: row+1, 
															weight:30, 
															name:"", 
															birthDate:"",
															gender:"unknown", 
															status:"live", 
															diseases:"0",
															content:"",  
															affection1:"", 
															affection2:"", 
															affection3:"", 
															affection4:"",
															image:"http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?gender=unknown&status=live&diseases=0"
															},
											          position: {x: position_x, y: position_y}}, 
							                       { group: "edges", 
							                    	 data: {source: colNewSon+"_"+row+1, target: colNewSon+"_"+row } }
							                       ]);
							    var options = {
							    		name: 'grid',
							    		rows: 15, // force num of rows in the grid
							    		columns: 15, // force num of cols in the grid 
							    		animate: true, // whether to transition the node positions
							    	    animationDuration: 2000, // duration of animation in ms if enabled
							    	    autolock: true,
							    		position: function(node){ return {row:node.data('row'), col:node.data('col')};},
							    };						
								$("#newPatientModal").modal({backdrop: "static"});
								var layout = cy.makeLayout(options);
							    layout.run();
							    cy.elements().lock();
								
							} else { // No tiene hijos
								marriedNode = getMarriedNode(node);
								var row = marriedNode.data('row');
								var col = marriedNode.data('col');
								var position_x = marriedNode.position('x');
								var position_y = marriedNode.position('y');
								
								
								var new_id = col+"_"+row+2;
								document.getElementById('myNode').value = '#'+new_id;
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id:col+"_"+row+1,
															col: col,
															row: row+1, 
															weight:0, 
															image:"svgs/pixel.svg"
															},
									                 position: {x: position_x, y: position_y}},
							                       { group: "edges", 
							                    	 data: { source: col+"_"+row+1, target: marriedNode.id() } }
							                       ]);
							    var eles = cy.add([
							                       { group: "nodes", 
							                    	 data: { id: col+"_"+row+2,
															col: col,
															row: row+2, 
															weight:30, 
															name:"", 
															birthDate:"",
															gender:"unknown", 
															status:"live", 
															diseases:"0",
															content:"",  
															affection1:"", 
															affection2:"", 
															affection3:"", 
															affection4:"",
															image:"http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?gender=unknown&status=live&diseases=0"
															},
											          position: {x: position_x, y: position_y}}, 
							                       { group: "edges", 
							                    	 data: {source: col+"_"+row+2, target: col+"_"+row+1 } }
							                       ]);
							    
							    var options = {
							    		name: 'grid',
							    		rows: 15, // force num of rows in the grid
							    		columns: 15, // force num of cols in the grid 
							    		animate: true, // whether to transition the node positions
							    	    animationDuration: 2000, // duration of animation in ms if enabled
							    	    autolock: true,
							    		position: function(node){ return {row:node.data('row'), col:node.data('col')};},
							    };						
								$("#newPatientModal").modal({backdrop: "static"});
								var layout = cy.makeLayout(options);
							    layout.run();
							    cy.elements().lock();
								
							}
						} else {
							alert ("Patient has to be married (implemented soon)");
						}
					},
					//disabled: true
				},
				{
					content: '<span class="fa fa-star fa-x">Sib</span>',
					select: function(){
						console.log( this.data('name') );
					},
					disabled: true
				},
				{
					content: '<span class="fa fa-times">Delete</span>',
					select: function(){
											
						var node = cy.$('#'+this.id());
						if (!hasSons(node)) {
							var directlyConnected = node.neighborhood();
							directlyConnected.forEach(function( ele ){
								if ((ele.isNode())&&!(parentSameCol(ele))) {
									cy.remove(ele);
								} 
							});
							cy.remove(node);
						} else if (!hasParents(node)) {
							cy.remove(node);
						}
						
					},
				}
				
			]
		});

      //--------------------------


      cy.elements().lock();
      //cy.zoomingEnabled( false );
      //cy.panningEnabled( false );
      
      
    //---------------------------------------------------
      cy.on('doubleTap', 'node', function(evt) {
    	  document.getElementById('myNode').value = '#'+this.id();
    	  cy.startBatch();
	        var res;
			var node = cy.$('#'+this.id());
			var name = node.data('name');
			var gender = node.data('gender');
			var birthdate = node.data('birthDate');
			var status = node.data('status');
			var diseases = node.data('diseases');
			var image = node.data('image');
			var affection1 = node.data('affection1');
			var affection2 = node.data('affection2');
			var affection3 = node.data('affection3');
			var affection4 = node.data('affection4');
			//alert("NACE: "+birthdate);
			$('#cpButton1').colorpicker({showOn:'button'});
			$('#cpButton2').colorpicker({showOn:'button'});
			$('#cpButton3').colorpicker({showOn:'button'});
			$('#cpButton4').colorpicker({showOn:'button'});
			
			document.getElementById('txname').value = name;
			document.getElementById('txdate').value = birthdate;
			$("#"+gender).prop("checked", true);
			$("#"+status).prop("checked", true);
			document.getElementById('affection1').value = affection1;
			document.getElementById('affection2').value = affection2;
			document.getElementById('affection3').value = affection3;
			document.getElementById('affection4').value = affection4;

			$("#editInformationModal").modal({backdrop: "static"});
			cy.endBatch();
      });
    }

  });
  
  var cy = $('#cy').cytoscape('get');
  var tappedBefore;
  var tappedTimeout;
  cy.on('tap', function(event) {
    var tappedNow = event.cyTarget;
    if (tappedTimeout && tappedBefore) {
      clearTimeout(tappedTimeout);
    }
    if(tappedBefore === tappedNow) {
      tappedNow.trigger('doubleTap');
      tappedBefore = null;
    } else {
      tappedTimeout = setTimeout(function(){ tappedBefore = null; }, 300);
      tappedBefore = tappedNow;
    }
  });
  

  
  
}); 

