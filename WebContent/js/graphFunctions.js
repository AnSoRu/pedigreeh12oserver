function removeNode() {
	var cy = window.cy;
	cy.startBatch();
	var id = document.getElementById('myNode').value;
	var node = cy.$(id);
	cy.remove(node);
	cy.endBatch();
}

function cleanModal() {
	document.getElementById('myNode').value = "";
	document.getElementById('txname1').value = "";
	document.getElementById('txdate1').value = "";
	$("#live1").prop("checked", true);
	$("#unknown1").prop("checked", true);
	document.getElementById('affection11').value = "";
	document.getElementById('affection21').value = "";
	document.getElementById('affection31').value = "";
	document.getElementById('affection41').value = "";
}
function updateNode() {
	//alert("ALAAAAA");
	var cy = window.cy;
	//alert(document.getElementById('myNode').value);
	//alert(cy);
	cy.startBatch();

	var id = document.getElementById('myNode').value;
	var node = cy.$(id);
	//document.getElementById('myNode').value = '';

	var name = document.getElementById('txname').value;
	cy.$(id).data('name', name);
	var gender = $('input[name=gender]:checked').attr('value');
	cy.$(id).data('gender', gender);
	var birthdate = document.getElementById('txdate').value;
	var status = $('input[name=status]:checked').attr('value');
	
	var affection1 = document.getElementById('affection1').value;
	var affection2 = document.getElementById('affection2').value;
	var affection3 = document.getElementById('affection3').value;
	var affection4 = document.getElementById('affection4').value;
	
	var colores='';
	var diseases = 0;
	if (affection1!="") {
		var color1 =document.getElementById('cpButton1').value;
		diseases++;
		colores=colores+"&color1="+color1;
	}
	if (affection2!="") {
		var color2 =document.getElementById('cpButton2').value;
		diseases++;
		colores=colores+"&color2="+color2;
	}
	if (affection3!="") {
		var color3 =document.getElementById('cpButton3').value;
		diseases++;
		colores=colores+"&color3="+color3;
	}
	if (affection4!="") {
		var color4 =document.getElementById('cpButton4').value;
		diseases++;
		colores=colores+"&color4="+color4;
	}
	var url = "http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?"+
	"gender="+gender+"&status="+status+"&diseases="+diseases+colores;
	//alert (name+"-"+gender+"-"+birthdate+"-"+status+"-"+diseases);

	// replace all #, not valid in url
	url = url.replace(/#/g, '');

	cy.$(id).data('name', name);
	cy.$(id).data('gender', gender);
	cy.$(id).data('status', status);
	cy.$(id).data('birthDate', birthdate);
	cy.$(id).data('diseases', diseases);
	cy.$(id).data('affection1', affection1);
	cy.$(id).data('affection2', affection2);
	cy.$(id).data('affection3', affection3);
	cy.$(id).data('affection4', affection4);
	cy.$(id).data('image', url);
	
	cy.endBatch();
	cleanModal();
}

function exportImg() {
	alert("se mete");
	
	canvas = $("canvas").find("[data-id='layer0-selectbox']");
	alert(canvas);
	//var cy = window.cy;
	//var png64 = cy.jpg();
	
	//var img = new Image();
	//img.setAttribute('crossOrigin', 'anonymous');
	//$('#png-eg').attr('src', png64);
	//img.src = png64;
	 //img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUA'+
     //'AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO'+
     //'9TXL0Y4OHwAAAABJRU5ErkJggg==';
	
	
	canvas.toBlob(function(blob) {
	    saveAs(blob, "pretty image.png");
	}, "image/png");

	
	/*if (canvas.toBlob) {
	    canvas.toBlob(
	        function (blob) {
	            // Do something with the blob object,
	            // e.g. creating a multipart form for file uploads:
	            var formData = new FormData();
	            formData.append('file', blob, "pretty image.png");
	        },
	        'image/png'
	    );
	}*/
	//window.location.href = img.src.replace('image/jpg', 'image/octet-stream');
	
}

function getMarriedNode(node) {
	var row = node.data('row');
	var directlyConnected = node.neighborhood();
	var result;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			//alert("NODO: "+ ele.id() );
			//alert ("INICIAL: "+ row);
			var row_obj = ele.data('row');
			if (row==row_obj) {
				result = ele;
			}
		} 
	});
	return result;
}

function hasMarried(node) {
	var row = node.data('row');
	var directlyConnected = node.neighborhood();
	var result = false;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			//alert("NODO: "+ ele.id() );
			//alert ("INICIAL: "+ row);
			var row_obj = ele.data('row');
			if (row==row_obj) {
				result = true;
			}
		} 
	});
	return result;
}

function hasSons(node) {
	var row = node.data('row');
	var directlyConnected = node.neighborhood();
	var result = false;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			//alert("NODO: "+ ele.id() );
			//alert ("INICIAL: "+ row);
			var undirectlyConnected = ele.neighborhood();
			undirectlyConnected.forEach(function( ele ){
				if (ele.isNode()) {
					var row_obj = ele.data('row');
					if (row<row_obj) {
						result=true;
					}
				}
			});
		} 
	});
	return result;
}

function getGestationNode(node) {
	var col = node.data('col');
	var directlyConnected = node.neighborhood();
	var result;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			//alert("NODO: "+ ele.id() );
			//alert ("INICIAL: "+ row);
			var col_obj = ele.data('col');
			if (col==col_obj) {
				result = ele;
			}
		} 
	});
	return result;
}

function getSpaceSon(gestationNode) {
	var col = gestationNode.data('col');
	var row = gestationNode.data('row');
	//alert("Gestation col: "+col + " - Row: "+row);
	//var result;
	var exist = false;
	var i = 1;
	while (!exist) {
		
		var col_aux = col+i;
		//alert(col_aux);
		var node1 = cy.$('#'+ col_aux +"_" + row);
		if (node1.isNode()) {
			i++;
		} else {
			
			exist = true;
		}
	}
	
	return col+i;
}

function hasParents(node) {
	var row = node.data('row');
	var directlyConnected = node.neighborhood();
	var result = false;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			//alert("NODO: "+ ele.id() );
			//alert ("INICIAL: "+ row);
			var undirectlyConnected = ele.neighborhood();
			undirectlyConnected.forEach(function( ele ){
				if (ele.isNode()) {
					var row_obj = ele.data('row');
					if (row>row_obj) {
						result=true;
					}
				}
			});
		} 
	});
	return result;
}

function parentSameCol(node) {
	var col = node.data('col');
	var row = node.data('row');
	var directlyConnected = node.neighborhood();
	var result = false;
	
	directlyConnected.forEach(function( ele ){
		if (ele.isNode()) {
			var col_obj = ele.data('col');
			var row_obj = ele.data('row');
			if ((col==col_obj)&&(row>row_obj)) {
				result=true;
			}
		} 
	});
	return result;
}

function hasSpace (c1, r1, c2, r2) {
	var result = true;
	//alert(c1 +"_"+r1+"_"+c2+"_"+r2);
	var node1 = cy.$('#'+ c1 +"_" + r1);
	var node2 = cy.$('#'+ c2 +"_" + r2);
	if ((node1.isNode())||(node2.isNode())) {
		result = false;
	}
	return result;
	
}

function createNode() {
	var cy = window.cy;
	//alert(document.getElementById('myNode').value);
	//alert(cy);
	cy.startBatch();

	var id = document.getElementById('myNode').value;
	//alert(id);
	var node = cy.$(id);
	//document.getElementById('myNode').value = '';

	var name = document.getElementById('txname1').value;
	cy.$(id).data('name', name);
	var gender = $('input[name=gender1]:checked').attr('value');
	cy.$(id).data('gender', gender);
	var birthdate = document.getElementById('txdate1').value;
	var status = $('input[name=status1]:checked').attr('value');
	
	var affection1 = document.getElementById('affection11').value;
	var affection2 = document.getElementById('affection21').value;
	var affection3 = document.getElementById('affection31').value;
	var affection4 = document.getElementById('affection41').value;
	
	var colores='';
	var diseases = 0;
	if (affection1!="") {
		var color1 =document.getElementById('cpButton11').value;
		diseases++;
		colores=colores+"&color1="+color1;
	}
	if (affection2!="") {
		var color2 =document.getElementById('cpButton21').value;
		diseases++;
		colores=colores+"&color2="+color2;
	}
	if (affection3!="") {
		var color3 =document.getElementById('cpButton31').value;
		diseases++;
		colores=colores+"&color3="+color3;
	}
	if (affection4!="") {
		var color4 =document.getElementById('cpButton41').value;
		diseases++;
		colores=colores+"&color4="+color4;
	}
	var url = "http://walter.dia.fi.upm.es:8080/PedigreeH12O/svgs/draw.jsp?"+
	"gender="+gender+"&status="+status+"&diseases="+diseases+colores;
	//alert (name+"-"+gender+"-"+birthdate+"-"+status+"-"+diseases);

	// replace all #, not valid in url
	url = url.replace(/#/g, '');

	cy.$(id).data('name', name);
	cy.$(id).data('gender', gender);
	cy.$(id).data('status', status);
	cy.$(id).data('birthDate', birthdate);
	cy.$(id).data('diseases', diseases);
	cy.$(id).data('affection1', affection1);
	cy.$(id).data('affection2', affection2);
	cy.$(id).data('affection3', affection3);
	cy.$(id).data('affection4', affection4);
	cy.$(id).data('image', url);
	
	cy.endBatch();
	cleanModal();
}