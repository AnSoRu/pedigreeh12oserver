<%@ page language="java" contentType="image/svg+xml; charset=UTF-8"
    pageEncoding="UTF-8"%><?xml version="1.0"?>
<svg width="90" height="90" xmlns="http://www.w3.org/2000/svg">
	<g>
	<!-- GENDER -->
		<!-- FEMALE -->
		<% if ( request.getParameter("gender").equals("female")){  %>
			<circle cx="45" cy="45" r="44" stroke="grey" fill="white" id="svg_1"/>
			<!-- AFFECTIONS -->
			<!-- 1 -->
			<% if ( request.getParameter("diseases").equals("1")){  %>
				<path opacity="0.5" id="svg_17" 
				d="m1.283017,45.066753l0,0c0,-24.278214 20.128985,-43.959607 44.960082,-43.959607l0,21.980113c-12.414955,0 -22.479612,9.840551 -22.479612,21.979494l-22.480469,0z" 
				stroke="#4c4c4c" 
				fill="#<%=request.getParameter("color1")%>"/>
			<%}%>
			<!-- 2-->
			<% if ( request.getParameter("diseases").equals("2")){  %>
				<path opacity="0.5" id="svg_17" 
				d="m1.283017,45.066753l0,0c0,-24.278214 20.128985,-43.959607 44.960082,-43.959607l0,21.980113c-12.414955,0 -22.479612,9.840551 -22.479612,21.979494l-22.480469,0z" 
				stroke="#4c4c4c" fill="#<%=request.getParameter("color1")%>"/>
				<path transform="rotate(90, 67.2617, 23.0859)" id="svg_4" opacity="0.5"
				 d="m45.282791,44.066719l0,0c0,-23.173672 19.681282,-41.959661 43.960106,-41.959661l0,20.980129c-12.13884,0 -21.97963,9.392855 -21.97963,20.979532l-21.980476,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color2")%>"/>
				
			<%}%>
			<!-- 3 -->
			<% if ( request.getParameter("diseases").equals("3")){  %>
				<path opacity="0.5" id="svg_17" 
				d="m1.283017,45.066753l0,0c0,-24.278214 20.128985,-43.959607 44.960082,-43.959607l0,21.980113c-12.414955,0 -22.479612,9.840551 -22.479612,21.979494l-22.480469,0z" 
				stroke="#4c4c4c" 
				fill="#<%=request.getParameter("color1")%>"/>
				<path transform="rotate(90, 67.2617, 23.0859)" id="svg_4" opacity="0.5"
				 d="m45.282791,44.066719l0,0c0,-23.173672 19.681282,-41.959661 43.960106,-41.959661l0,20.980129c-12.13884,0 -21.97963,9.392855 -21.97963,20.979532l-21.980476,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color2")%>"/>
				 <path transform="rotate(-90, 23.3828, 67.084)" id="svg_7" opacity="0.5" 
				 d="m1.901331,88.564423l0,0c0,-23.725952 19.233585,-42.959641 42.960123,-42.959641l0,21.480118c-11.862694,0 -21.479654,9.616699 -21.479654,21.479523l-21.480469,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color3")%>"/>
				
			<%}%>
			<!-- 4+ -->
			<% if (request.getParameter("diseases").equals("4")){  %>
				<path opacity="0.5" id="svg_17" 
				d="m1.283017,45.066753l0,0c0,-24.278214 20.128985,-43.959607 44.960082,-43.959607l0,21.980113c-12.414955,0 -22.479612,9.840551 -22.479612,21.979494l-22.480469,0z" 
				stroke="#4c4c4c" 
				fill="#<%=request.getParameter("color1")%>"/>
				<path transform="rotate(90, 67.2617, 23.0859)" id="svg_4" opacity="0.5"
				 d="m45.282791,44.066719l0,0c0,-23.173672 19.681282,-41.959661 43.960106,-41.959661l0,20.980129c-12.13884,0 -21.97963,9.392855 -21.97963,20.979532l-21.980476,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color2")%>"/>
				 <path transform="rotate(-90, 23.3828, 67.084)" id="svg_7" opacity="0.5" 
				 d="m1.901331,88.564423l0,0c0,-23.725952 19.233585,-42.959641 42.960123,-42.959641l0,21.480118c-11.862694,0 -21.479654,9.616699 -21.479654,21.479523l-21.480469,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color3")%>"/>
				 <path id="svg_6" transform="rotate(180, 66.8828, 67.0859)" opacity="0.5" 
				 d="m44.901855,88.066719l0,0c0,-23.173668 19.681282,-41.959663 43.960106,-41.959663l0,20.980133c-12.13884,0 -21.97963,9.392853 -21.97963,20.97953l-21.980476,0z" 
				 stroke="#4c4c4c" fill="#<%=request.getParameter("color4")%>"/>
				
			<%}%>
			<!-- 0 -->
			
		<!-- MALE -->
		<% } else if ( request.getParameter("gender").equals("male")){  %>
			<rect id="rect" x= "4"  y="4" width="84" height="84" fill="white" stroke-width="1" stroke= "grey" />
			<!-- AFFECTIONS -->
			<% if ( request.getParameter("diseases").equals("1")){  %>
				<g id="affection01" opacity="0.5">
				   <g id="svg_1">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				   </g>
				   <g id="svg_0">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				   </g>
				</g>
			<%}%>
			<!-- 2-->
			<% if ( request.getParameter("diseases").equals("2")){  %>
				<g id="affection01" opacity="0.5">
				   <g id="svg_1">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				   </g>
				   <g id="svg_0">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				   </g>
				</g>
				<g id="affection02" opacity="0.5">
				   <g id="svg_9">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" width="24.499999" height="23.499999" id="svg_10"/>
				   </g>
				   <g id="svg_11">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.499999" height="23.499999" id="svg_12"/>
				   </g>
				 </g>
			<%}%>
			<!-- 3 -->
			<% if ( request.getParameter("diseases").equals("3")){  %>
				<g id="affection01" opacity="0.5">
				   <g id="svg_1">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				   </g>
				   <g id="svg_0">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				   </g>
				</g>
				<g id="affection02" opacity="0.5">
				   <g id="svg_9">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" width="24.499999" height="23.499999" id="svg_10"/>
				   </g>
				   <g id="svg_11">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.499999" height="23.499999" id="svg_12"/>
				   </g>
				 </g>
				 <g id="affection03" opacity="0.5">
				   <g id="svg_5">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" width="24.499999" height="24.499999" id="svg_6"/>
				   </g>
				   <g id="svg_7">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" stroke="#808080" width="24.499999" height="24.499999" id="svg_8"/>
				   </g>
				  </g>
			<%}%>
			<!-- 4+ -->
			<% if ( request.getParameter("diseases").equals("4")){  %>
				<g id="affection01" opacity="0.5">
				   <g id="svg_1">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				   </g>
				   <g id="svg_0">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				   </g>
				</g>
				<g id="affection02" opacity="0.5">
				   <g id="svg_9">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" width="24.499999" height="23.499999" id="svg_10"/>
				   </g>
				   <g id="svg_11">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.499999" height="23.499999" id="svg_12"/>
				   </g>
				 </g>
				 <g id="affection03" opacity="0.5">
				   <g id="svg_5">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" width="24.499999" height="24.499999" id="svg_6"/>
				   </g>
				   <g id="svg_7">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" stroke="#808080" width="24.499999" height="24.499999" id="svg_8"/>
				   </g>
				  </g>
				  <g id="affection04" opacity="0.5">
					   <g id="svg_13">
					    <rect x="63.015002" y="63.164001" fill="#<%=request.getParameter("color4")%>" width="24.499999" height="24.499999" id="svg_14"/>
					   </g>
					   <g id="svg_15">
					    <rect x="63.015002" y="63.164001" fill="#<%=request.getParameter("color4")%>" stroke="#808080" width="24.499999" height="24.499999" id="svg_16"/>
					   </g>
				  </g>
			<%}%>
			<!-- 0 -->
		<!-- GENDER UNKNOWN -->
		<% }  else {%>
		<rect transform="rotate(135.84, 45.6369, 44.7601)" stroke="grey" fill="white" height="60.856633" width="60.856633" y="14.331735" x="15.208609" id="rect"/>
			<!-- AFFECTIONS -->
			<% if ( request.getParameter("diseases").equals("1")){  %>
				<g transform="rotate(135.184, 19.75, 43.75)" opacity="0.5" id="XMLID_1_">
					<g id="svg_1">
					 <rect id="svg_2" height="23.499999" width="25.500001" 
					 fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					</g>
					<g id="svg_3">
					 <rect id="svg_4" height="23.499999" width="25.500001" stroke="#808080" 
					 fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					</g>
				  </g>

			<%}%>
			<!-- 2-->
			<% if ( request.getParameter("diseases").equals("2")){  %>
				<g id="XMLID_1_" opacity="0.5" transform="rotate(135.184, 19.75, 43.75)">
				 <g id="svg_1">
				  <rect x="7" y="32" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				 </g>
				 <g id="svg_3">
				  <rect x="7" y="32" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				 </g>
				</g>
				<g transform="rotate(134.985, 45.599, 19.75)" opacity="0.5" id="XMLID_3_">
				 <g id="svg_9">
				  <rect id="svg_10" height="23.499999" width="24.499999" 
				  fill="#<%=request.getParameter("color2")%>" y="8" x="33.349001"/>
				 </g>
				 <g id="svg_11">
				  <rect id="svg_12" height="23.499999" width="24.499999" stroke="#808080" 
				  fill="#<%=request.getParameter("color2")%>" y="8" x="33.349001"/>
				 </g>
				</g>
			<%}%>
			<!-- 3 -->
			<% if ( request.getParameter("diseases").equals("3")){  %>
				 <g transform="rotate(135.184, 19.75, 43.75)" opacity="0.5" id="XMLID_1_">
					 <g id="svg_1">
					  <rect id="svg_2" height="23.499999" width="25.500001" 
					  fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					 </g>
					 <g id="svg_3">
					  <rect id="svg_4" height="23.499999" width="25.500001" stroke="#808080" 
					  fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					 </g>
					</g>
					<g id="XMLID_3_" opacity="0.5" transform="rotate(134.985, 45.599, 19.75)">
					 <g id="svg_9">
					  <rect x="33.349001" y="8" fill="#<%=request.getParameter("color2")%>" width="24.499999" height="23.499999" id="svg_10"/>
					 </g>
					 <g id="svg_11">
					  <rect x="33.349001" y="8" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.499999" height="23.499999" id="svg_12"/>
					 </g>
					</g>
					<g transform="rotate(135.247, 45.249, 70.419)" opacity="0.5" id="XMLID_2_">
					 <g id="svg_5">
					  <rect id="svg_6" height="24.499999" width="24.499999" 
					  fill="#<%=request.getParameter("color3")%>" y="58.169002" x="32.999"/>
					 </g>
					 <g id="svg_7">
					  <rect id="svg_8" height="24.499999" width="24.499999" stroke="#808080" 
					  fill="#<%=request.getParameter("color3")%>" y="58.169002" x="32.999"/>
					 </g>
					</g>
			<%}%>
			<!-- 4+ -->
			<% if ( request.getParameter("diseases").equals("4")){  %>
				<g transform="rotate(135.184, 19.75, 43.75)" opacity="0.5" id="XMLID_1_">
					 <g id="svg_1">
					  <rect id="svg_2" height="23.5" width="25.5" fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					 </g>
					 <g id="svg_3">
					  <rect id="svg_4" height="23.5" width="25.5" stroke="#808080" 
					  fill="#<%=request.getParameter("color1")%>" y="32" x="7"/>
					 </g>
					</g>
					<g id="XMLID_3_" opacity="0.5" transform="rotate(134.985, 45.599, 19.75)">
					 <g id="svg_9">
					  <rect x="33.349" y="8" fill="#<%=request.getParameter("color2")%>" width="24.5" height="23.5" id="svg_10"/>
					 </g>
					 <g id="svg_11">
					  <rect x="33.349" y="8" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.5" height="23.5" id="svg_12"/>
					 </g>
					</g>
					<g transform="rotate(135.247, 45.249, 70.419)" opacity="0.5" id="XMLID_2_">
					 <g id="svg_5">
					  <rect id="svg_6" height="24.5" width="24.5" fill="#<%=request.getParameter("color3")%>" y="58.169" x="32.999"/>
					 </g>
					 <g id="svg_7">
					  <rect id="svg_8" height="24.5" width="24.5" stroke="#808080" 
					  fill="#<%=request.getParameter("color3")%>" y="58.169" x="32.999"/>
					 </g>
					</g>
					<g transform="rotate(134.935, 71.265, 45.414)" opacity="0.5" id="XMLID_4_">
					 <g id="svg_13">
					  <rect id="svg_14" height="24.499999" width="24.499999" fill="#<%=request.getParameter("color4")%>" y="33.164001" x="59.015002"/>
					 </g>
					 <g id="svg_15">
					  <rect id="svg_16" height="24.499999" width="24.499999" stroke="#808080" fill="#<%=request.getParameter("color4")%>" y="33.164001" x="59.015002"/>
					 </g>
					</g>
			
			
				<g id="affection01" opacity="0.5">
				   <g id="svg_1">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" width="25.500001" height="23.499999" id="svg_2"/>
				   </g>
				   <g id="svg_0">
				    <rect x="4" y="4" fill="#<%=request.getParameter("color1")%>" stroke="#808080" width="25.500001" height="23.499999" id="svg_4"/>
				   </g>
				</g>
				<g id="affection02" opacity="0.5">
				   <g id="svg_9">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" width="24.499999" height="23.499999" id="svg_10"/>
				   </g>
				   <g id="svg_11">
				    <rect x="63.349001" y="4" fill="#<%=request.getParameter("color2")%>" stroke="#808080" width="24.499999" height="23.499999" id="svg_12"/>
				   </g>
				 </g>
				 <g id="affection03" opacity="0.5">
				   <g id="svg_5">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" width="24.499999" height="24.499999" id="svg_6"/>
				   </g>
				   <g id="svg_7">
				    <rect x="3.999" y="63.169002" fill="#<%=request.getParameter("color3")%>" stroke="#808080" width="24.499999" height="24.499999" id="svg_8"/>
				   </g>
				  </g>
				  <g id="affection04" opacity="0.5">
					   <g id="svg_13">
					    <rect x="63.015002" y="63.164001" fill="#<%=request.getParameter("color4")%>" width="24.499999" height="24.499999" id="svg_14"/>
					   </g>
					   <g id="svg_15">
					    <rect x="63.015002" y="63.164001" fill="#<%=request.getParameter("color4")%>" stroke="#808080" width="24.499999" height="24.499999" id="svg_16"/>
					   </g>
				  </g>
			<%}%>
		
		<% } %>
		<!-- STATUS -->
		<% if ( request.getParameter("status").equals("dead")){  %>
			<line id="linea" x1="94" y1="-2.07" x2="-2.5" y2="96.06" style="stroke:black;stroke-width:5" />  
		<% }  %>  
		  
		
	</g>
</svg>