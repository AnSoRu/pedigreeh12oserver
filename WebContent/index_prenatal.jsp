<!DOCTYPE html>
<%@page contentType="text/html; charset=utf-8"
	import="pedigree.gib.upm.Patient"%>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Pedrigree H12O</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="css/jquery.auto-complete.css">
<!-- Custom Fonts -->
<!-- <link href="font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="css/style.css" rel="stylesheet" />
<link href="css/style_menu.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.0/jquery.qtip.css">
<link id="jquiCSS" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-lightness/jquery-ui.css" type="text/css" media="all">
<link href="css/evol.colorpicker.min.css" rel="stylesheet" />
<link href="css/modal.css" rel="stylesheet" />
<link rel="stylesheet" href="css/awesomplete.css" />


</head>

<body>
	<img id="png-eg" src="">
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">Pedigree H12O</a>
			</div>
			<!-- Top Menu Items -->
			<ul class="nav navbar-right top-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-save"></i> <b
						class="caret"></b></a>
					<ul class="dropdown-menu alert-dropdown">
						<li><a href="#" onclick="exportImg()">Export file <i
								class="fa fa-upload"></i></a></li>
						<li><a href="#">Import file <i class="fa fa-download"></i></a>
						</li>
					</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-bell"></i> <b
						class="caret"></b></a>
					<ul class="dropdown-menu alert-dropdown">

						<li><a href="#">Pedigree patient 235 <span
								class="label label-success">Check</span></a></li>
						<li><a href="#">Pedigree patient 12312 <span
								class="label label-warning">Warning</span></a></li>
						<li><a href="#">Pedigree patient xxxx <span
								class="label label-danger">Urgent</span></a></li>
						<li class="divider"></li>
						<li><a href="#">View All</a></li>
					</ul></li>

				<%!
					String user;
				%>
				<%
					//String user = request.getParameter("user");
				      String user = request.getUserPrincipal().getName();
				%>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-user"></i> <%=user%> <b
						class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="<%=request.getContextPath()%>/josso_logout/"><i class="fa fa-fw fa-power-off"></i> Log
								Out</a></li>
					</ul></li>
			</ul>
			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li class="active"><a href="index.jsp"><i
							class="fa fa-fw fa-sitemap"></i> Select Patient</a></li>
					<li><a href="javascript:;" data-toggle="collapse"
						data-target="#demo"><i class="fa fa-fw fa-sitemap"></i> Select
							Template <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="demo" class="collapse">
							<li><a href="index_clinica.jsp">Clinical</a></li>
							<li><a href="index_prenatal.jsp">Prenatal</a></li>
							<li><a href="index_example.jsp">Other</a></li>
						</ul></li>
					<li><a href="#"><i class="fa fa-fw fa-file"></i> Help</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</nav>

		<div id="page-wrapper">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default"
						style="height: 650px; width: 100%;">
						<div class="panel-heading">
							<h3 class="panel-title">
								<a id="refresh-button" href="#"><i class="glyphicon glyphicon-refresh"></i></a>
								&nbsp; &nbsp; Pedigree
								
							</h3>
						</div>
						<div class="panel-body">
							<div id="cy"></div>

<!-- MODAL DE EDITAR NODO -->
							<div class="modal fade" id="editInformationModal" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4>Edit Patient Information</h4>
											<input id="myNode" value="" type="hidden" />
										</div>

										<div class="modal-body">
											<form role="form">
												<table style="width:550px">
													<tr style="width:100%">
														<td width="22%">Name:</td>
														<td align="center" valign="middle"><input id="txname"
															type="text" name="name" style="width: 100%" /></td>
													</tr>
													<tr>
														<td>Gender:</td>
														<td align="left" valign="middle"><input type="radio"
															id="male" name="gender" value="male" /> Male <input
															type="radio" id="female" name="gender" value="female" />
															Female <input type="radio" id="unknown" name="gender"
															value="unknown" checked /> Unknown</td>
													</tr>
													<tr>
														<td>Birth date:</td>
														<td><input id="txdate" style="width: 100%"
															type="text" name="bday" value="" /></td>
													</tr>
													<tr>
														<td>Status:</td>
														<td align="left" valign="middle"><input type="radio"
															id="live" name="status" value="live" checked /> Live <input
															type="radio" id="dead" name="status" value="dead" />
															Dead</td>
													</tr>
													<tr>
														<td><input id="cpButton1" value="#00b0f0"
															type="hidden" /> &nbsp; 1st Affection:</td>
														<td><input id="affection1" style="width: 100%"
															type="text" name="affection1" /></td>
													</tr>
													<tr>
														<td><input id="cpButton2" value=#ff0000 type="hidden" />
															&nbsp; 2nd Affection:</td>
														<td><input id="affection2" style="width: 100%"
															type="text" name="affection2" /></td>
													</tr>
													<tr>
														<td><input id="cpButton3" value="#00b050"
															type="hidden" />&nbsp; 3rd Affection:</td>
														<td><input id="affection3" style="width: 100%"
															type="text" name="affection3" /></td>
													</tr>
													<tr>
														<td><input id="cpButton4" value="#ffff00"
															type="hidden" />&nbsp; 4th Affection:</td>
														<td><input id="affection4" style="width: 100%"
															type="text" name="affection4" /></td>
													</tr>
												</table>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal" onclick="updateNode()">
												<span class="glyphicon glyphicon-plus-sign"></span> Save
											</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal" onclick="removeNode()">
												<span class="glyphicon glyphicon-remove-sign"></span> Remove
											</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
							
							<!-- MODAL PARA CREACION DE NODO -->
							<div class="modal fade" id="newPatientModal" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4>Enter new Patient Information</h4>
											<input id="myNode" value="" type="hidden" />
										</div>

										<div class="modal-body">
											<form role="form">
												<table style="width:550px">
													<tr style="width:100%">
														<td width="22%">Name:</td>
														<td align="center" valign="middle"><input id="txname1"
															type="text" name="name" style="width: 100%" /></td>
													</tr>
													<tr>
														<td>Gender:</td>
														<td align="left" valign="middle"><input type="radio"
															id="male1" name="gender1" value="male" /> Male <input
															type="radio" id="female1" name="gender1" value="female" />
															Female <input type="radio" id="unknown1" name="gender1"
															value="unknown" checked /> Unknown</td>
													</tr>
													<tr>
														<td>Birth date:</td>
														<td><input id="txdate1" style="width: 100%"
															type="text" name="bday" value="" /></td>
													</tr>
													<tr>
														<td>Status:</td>
														<td align="left" valign="middle"><input type="radio"
															id="live1" name="status1" value="live" checked /> Live <input
															type="radio" id="dead1" name="status1" value="dead" />
															Dead</td>
													</tr>
													<tr>
														<td><input id="cpButton11" value="#00b0f0"
															type="hidden" /> &nbsp; 1st Affection:</td>
														<td><input id="affection11" style="width: 100%"
															type="text" name="affection11" /></td>
													</tr>
													<tr>
														<td><input id="cpButton21" value=#ff0000 type="hidden" />
															&nbsp; 2nd Affection:</td>
														<td><input id="affection21" style="width: 100%"
															type="text" name="affection21" /></td>
													</tr>
													<tr>
														<td><input id="cpButton31" value="#00b050"
															type="hidden" />&nbsp; 3rd Affection:</td>
														<td><input id="affection31" style="width: 100%"
															type="text" name="affection31" /></td>
													</tr>
													<tr>
														<td><input id="cpButton41" value="#ffff00"
															type="hidden" />&nbsp; 4th Affection:</td>
														<td><input id="affection41" style="width: 100%"
															type="text" name="affection41" /></td>
													</tr>
												</table>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal" onclick="createNode()">
												<span class="glyphicon glyphicon-plus-sign"></span> Save
											</button>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>



			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->



	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="js/jquery.auto-complete.js"></script>
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.0/jquery.qtip.js"></script>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>-->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"
		type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<script src="js/cytoscape_250.js"></script>
	<script src="js/cytoscape-cxtmenu.js"></script>

	<script src="js/templates/nodos_prenatal.js"></script>
	<script src="js/templates/arcos_prenatal.js"></script>
	<script src="js/initGraph.js"></script>
	<script src="js/graphFunctions.js"></script>
	<script src="js/cytoscape-qtip.js"></script>

	<script src="js/awesomplete.js"></script>
	<script src="js/evol.colorpicker.min.js" type="text/javascript"></script>

	<script>
		var ajax
		ajax = new XMLHttpRequest()
		ajax.open("GET", "files/vocabulary.json", true)
		ajax.onload = function() {
			var code;
			{
					var list = JSON.parse(ajax.responseText).map(function(i) {
						code = i.code;
					return i.name + " // " + i.vocabulary;
					})
					new Awesomplete(document.querySelector("#affection11"),{list:list});
					//alert(code);
					new Awesomplete(document.querySelector("#affection21"),{list:list});
					new Awesomplete(document.querySelector("#affection31"),{list:list});
					new Awesomplete(document.querySelector("#affection41"),{list:list});
					new Awesomplete(document.querySelector("#affection1"),{list:list});
					new Awesomplete(document.querySelector("#affection2"),{list:list});
					new Awesomplete(document.querySelector("#affection3"),{list:list});
					new Awesomplete(document.querySelector("#affection4"),{list:list});
		}
	}
		ajax.send();
	</script>

	
	
	<script>
	    // If the refresh-button is clicked in reality we will center the graph.
	    $("#refresh-button").click(function(e){
	        // $.get("/cluster/{{ cluster_slug }}/refresh/", function() {
	        //    fetchclusterdata()
	        var cy = window.cy;
	        //var node = cy.$("#1_1");
			cy.center();
			cy.fit();
				//cy.reset();
	        //});
	    })
  	</script>
</body>

</html>
