<%@page contentType="text/html; charset=UTF-8" language="java" session="true" %>
<!--
Redirects the user to the proper login page.  Configured as the login url the web.xml for this application.
-->
<%
    response.sendRedirect(request.getContextPath() + "/josso_login/");
%>