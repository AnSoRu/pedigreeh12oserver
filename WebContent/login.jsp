<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<html>
<head>
<!-- Login CSS -->
<link href="css/login.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pedigree H12O - Login</title>
</head>
<body>
	<div class="container">
		<div class="card card-container">
			<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
			<img id="profile-img" class="profile-img-card"
				src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
			<p id="profile-name" class="profile-name-card"></p>
			<form class="form-signin" method="post" action="index.jsp">
				<span id="reauth-email" class="reauth-email"></span> 
				<input type="text" id="inputEmail" class="form-control" placeholder="User" name="user" required autofocus>
				<input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
				<div id="remember" class="checkbox">
					<label> <input type="checkbox" value="remember-me">Remember me</label>
				</div>
				<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
			</form>
			
			<%
			String user = request.getParameter("user");
			String pass = request.getParameter("password");
			
			
			%>
			<!-- /form -->
			<a href="#" class="forgot-password"> Forgot the password? </a>
		</div>
		<!-- /card-container -->
	</div>
	<!-- /container -->
</body>
</html>