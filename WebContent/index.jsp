<!DOCTYPE html>
<%@page contentType="text/html; charset=utf-8"
	import="pedigree.gib.upm.Patient"%>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Pedrigree H12O</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Custom Fonts -->
<!-- <link href="font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link href="css/style.css" rel="stylesheet" />
<link href="css/style_menu.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.0/jquery.qtip.css">

</head>

<body>

	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">Pedigree H12O</a>
			</div>
			<!-- Top Menu Items -->
			<ul class="nav navbar-right top-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-save"></i> <b
						class="caret"></b></a>
					<ul class="dropdown-menu alert-dropdown">
						<li><a href="#" onclick="exportImg()">Export file <i class="fa fa-upload"></i></a>
						</li>
						<li><a href="#">Import file <i class="fa fa-download"></i></a>
						</li>
					</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-bell"></i> <b
						class="caret"></b></a>
					<ul class="dropdown-menu alert-dropdown">

						<li><a href="#">Pedigree patient 235 <span
								class="label label-success">Check</span></a></li>
						<li><a href="#">Pedigree patient 12312 <span
								class="label label-warning">Warning</span></a></li>
						<li><a href="#">Pedigree patient xxxx <span
								class="label label-danger">Urgent</span></a></li>
						<li class="divider"></li>
						<li><a href="#">View All</a></li>
					</ul></li>

				<%!
					String user;
				%>
				<%
					//String user = request.getParameter("user");
					String user = request.getUserPrincipal().getName();
				%>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"><i class="fa fa-user"></i> <%=user%> <b
						class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="<%=request.getContextPath()%>/josso_logout/"><i class="fa fa-fw fa-power-off"></i> Log
								Out</a></li>
					</ul></li>
			</ul>
			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li class="active"><a href="index.jsp"><i
							class="fa fa-fw fa-sitemap"></i> Select Patient</a></li>
					<li><a href="javascript:;" data-toggle="collapse"
						data-target="#demo"><i class="fa fa-fw fa-sitemap"></i>
							Select Template <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="demo" class="collapse">
							<li><a href="index_clinica.jsp">Clinical</a></li>
							<li><a href="index_prenatal.jsp">Prenatal</a></li>
							<li><a href="index_example.jsp">Other</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="fa fa-fw fa-file"></i>
							Help</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</nav>

		<div id="page-wrapper">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default"
						style="height: 650px; width: 100%;">
						<!-- <div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-pencil fa-fw"></i> Pedigree
							</h3>
						</div>-->
						<div class="panel-body">
							<div class="container marketing">

								<!-- Three columns of text below the carousel -->
								<div class="row">
									<div class="col-lg-4">
										<img width="140" height="140" alt="Generic placeholder image"
											src="images/clinica.jpg"
											>
										<h2>Clinical Template</h2>
										<p>Pedigree template in the clinical environment. This
											template is based on the study of a whole family; starting
											with the studied couple to the paternal grandparents and
											maternal</p>
										<p>
											<a role="button" href="pedigree.jsp?template=clinica" class="btn btn-default">View
												details »</a>
										</p>
									</div>
									<!-- /.col-lg-4 -->


									<div class="col-lg-4">
										<img width="140" height="140" alt="Generic placeholder image"
											src="images/prenatal.jpg">
										<h2>Prenatal Template</h2>
										<p>Pedigree template in the prenatal environment. This
											template is based on the study of pregnant women; starting
											with the studied couple and a child</p>
										<p>
											<a role="button" href="pedigree.jsp?template=prenatal" class="btn btn-default">View
												details »</a>
										</p>
									</div>
									<!-- /.col-lg-4 -->


									<div class="col-lg-4">
										<img width="140" height="140" alt="Generic placeholder image"
											src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
											class="img-circle">
										<h2>Other Template</h2>
										<p>Coming soon...</p>
										<p>
											<a role="button" href="pedigree.jsp?template=example" class="btn btn-default">View
												details »</a>
										</p>
									</div>
									<!-- /.col-lg-4 -->
								</div>
								<!-- /.row -->


								<!-- START THE FEATURETTES -->

								<hr class="featurette-divider">


							</div>
						</div>
					</div>
				</div>



			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.0/jquery.qtip.js"></script>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>


</body>

</html>
