package pedigree.gib.upm;

import java.util.ArrayList;

public class Patient {

	
	String id;
	String gender;
	ArrayList<String> diseases;
	
	public Patient(String id, String gender, ArrayList<String> diseases) {
		super();
		this.id = id;
		this.gender = gender;
		this.diseases = diseases;
	}
	
	public Patient(String id, String gender) {
		super();
		this.id = id;
		this.gender = gender;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public ArrayList<String> getDiseases() {
		return diseases;
	}
	public void setDiseases(ArrayList<String> diseases) {
		this.diseases = diseases;
	}
	
	
}
