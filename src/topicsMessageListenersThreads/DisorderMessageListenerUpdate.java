package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Disorder;
import flexjson.JSONDeserializer;
import model.DisorderDAO;

public class DisorderMessageListenerUpdate extends Thread implements MessageListener {

	private DisorderDAO dDao = new DisorderDAO();
	
	static final Logger dMLogger = LogManager.getLogger(DisorderMessageListenerUpdate.class.getName());
	
	public void onMessage(Message mess) {
		try {
			String messageStr = ((TextMessage)mess).getText();
			Disorder dAux = new JSONDeserializer<Disorder>().deserialize(messageStr);
			dMLogger.info("Trying to update disorder with ID = " + dAux.getDisorderCode());
			dDao.updateDisorderCode(dAux,new Date());
			dMLogger.info("Disorder with ID = " + dAux.getDisorderCode() + " updated correctly-");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
