package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Union;
import flexjson.JSONDeserializer;
import model.UnionDAO;

public class UnionMessageListenerInsert extends Thread implements MessageListener {

	private UnionDAO uDao = new UnionDAO();
	static final Logger uMLogger = LogManager.getLogger(UnionMessageListenerInsert.class.getName());
	
	public void onMessage(Message mess) {
		
		try {
			String messageStr = ((TextMessage)mess).getText();
			Union uAux = new JSONDeserializer<Union>().deserialize(messageStr);
			if(uDao.insertUnion(uAux)){
				uMLogger.info("Union with ID = " + uAux.getUnionId() + " inserted correctly.");
			}
			else{
				uMLogger.info("Fail when trying to insert union with ID = " + uAux.getUnionId());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
