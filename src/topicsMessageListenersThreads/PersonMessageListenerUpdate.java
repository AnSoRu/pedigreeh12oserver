package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Person;
import flexjson.JSONDeserializer;
import model.PersonDAO;

public class PersonMessageListenerUpdate extends Thread implements MessageListener {

	private PersonDAO pDao = new PersonDAO();
	
	static final Logger pMLogger = LogManager.getLogger(PersonMessageListenerUpdate.class.getName());
	
	public void onMessage(Message mess) {
		try {
			String messageStr = ((TextMessage)mess).getText();
			Person pAux = new JSONDeserializer<Person>().deserialize(messageStr);
			pMLogger.info("Trying to update person with ID = " + pAux.getClinicalhistoryid());
			pDao.updatePerson(pAux,new Date());
			pMLogger.info("Person with ID = " + pAux.getClinicalhistoryid() + " updated correctly.");
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
