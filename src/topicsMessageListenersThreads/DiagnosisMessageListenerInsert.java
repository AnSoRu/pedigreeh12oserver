package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Diagnosis;
import flexjson.JSONDeserializer;
import model.DiagnosisDAO;

public class DiagnosisMessageListenerInsert extends Thread implements MessageListener{
	
	private DiagnosisDAO dDao = new DiagnosisDAO();
	static final Logger dMLogger = LogManager.getLogger(DiagnosisMessageListenerInsert.class.getName());

	public void onMessage(Message message) {
		try {
			String messageStr = ((TextMessage)message).getText();
			Diagnosis dAux = new JSONDeserializer<Diagnosis>().deserialize(messageStr);
			if(dDao.insertDiagnosis(dAux)){
				dMLogger.info("Diagnosis with ID = " + dAux.getIdDiagnosis() + " inserted correctly.");
			}
			else{
				dMLogger.info("Fail when trying to insert diagnosis with ID = " + dAux.getIdDiagnosis());
			}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
