package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Gestation;
import flexjson.JSONDeserializer;
import model.GestationDAO;

public class GestationMessageListenerUpdate extends Thread implements MessageListener {

	private GestationDAO gDao = new GestationDAO();
	
	static final Logger gMLogger = LogManager.getLogger(GestationMessageListenerUpdate.class.getName());

	public void onMessage(Message mess) {
		
		try {
			String messageStr = ((TextMessage)mess).getText();
			Gestation gAux = new JSONDeserializer<Gestation>().deserialize(messageStr);
			gMLogger.info("Trying to update gestation with ID = " + gAux.getIdGestation());
			gDao.updateGestation(gAux,new Date());
			gMLogger.info("Gestation with ID = " + gAux.getIdGestation() + " updated correctly.");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
