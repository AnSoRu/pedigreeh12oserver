package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Disorder;
import flexjson.JSONDeserializer;
import model.DisorderDAO;

public class DisorderMessageListenerInsert extends Thread implements MessageListener {

	private DisorderDAO dDao = new DisorderDAO();
	static final Logger dMLogger = LogManager.getLogger(DisorderMessageListenerInsert.class.getName());
	
	public void onMessage(Message message) {
		
		try {
			String messageStr = ((TextMessage)message).getText();
			Disorder dAux = new JSONDeserializer<Disorder>().deserialize(messageStr);
			if(dDao.insertDisorderCode(dAux)){
				dMLogger.info("Disorder with ID = " + dAux.getDisorderCode() + " inserted correctly.");
			}
			else{
				dMLogger.info("Fail when trying to insert disorder with ID = " + dAux.getDisorderCode());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
