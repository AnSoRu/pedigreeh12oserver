package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Birth;
import flexjson.JSONDeserializer;
import model.BirthDAO;

public class BirthMessageListenerUpdate extends Thread implements MessageListener {

	private BirthDAO bdao = new BirthDAO();
	
	static final Logger bMLogger = LogManager.getLogger(BirthMessageListenerUpdate.class.getName());
	
	public void onMessage(Message message) {
		
		try {
			String messageStr = ((TextMessage)message).getText();
			Birth bAux = new JSONDeserializer<Birth>().deserialize(messageStr);
			bMLogger.info("Trying to update birth with ID = " + bAux.getBirthid());
			bdao.updateBirth(bAux,new Date());
			bMLogger.info("Birth with ID = " + bAux.getBirthid() + " updated correctly.");
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}

}
