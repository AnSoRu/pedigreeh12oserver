package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Diagnosis;
import flexjson.JSONDeserializer;
import model.DiagnosisDAO;

public class DiagnosisMessageListenerUpdate extends Thread implements MessageListener {

	private DiagnosisDAO dDao = new DiagnosisDAO();
	
	static final Logger dMLogger = LogManager.getLogger(DiagnosisMessageListenerUpdate.class.getName());
	
	public void onMessage(Message message) {
		
		try {
			String messageStr = ((TextMessage)message).getText();
			Diagnosis dAux = new JSONDeserializer<Diagnosis>().deserialize(messageStr);
			dMLogger.info("Trying to update diagnosis with ID = " + dAux.getIdDiagnosis());
			dDao.updateDiagnosis(dAux,new Date());
			dMLogger.info("Diagnosis with ID = " + dAux.getIdDiagnosis() + " updated correctly.");
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
