package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Person;
import flexjson.JSONDeserializer;
import model.PersonDAO;

public class PersonMessageListenerInsert extends Thread implements MessageListener {

	private PersonDAO pDao = new PersonDAO();
	static final Logger pMLogger = LogManager.getLogger(PersonMessageListenerInsert.class.getName());
	
	public void onMessage(Message mess) {
		try {
			String messageStr = ((TextMessage)mess).getText();
			Person pAux = new JSONDeserializer<Person>().deserialize(messageStr);
			if(pDao.insertPerson(pAux)){
				pMLogger.info("Person with ID = " + pAux.getClinicalhistoryid() + " inserted correctly.");
			}
			else{
				pMLogger.error("Fail when trying to insert person with ID = " + pAux.getClinicalhistoryid());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
