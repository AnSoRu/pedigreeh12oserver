package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Birth;
import flexjson.JSONDeserializer;
import model.BirthDAO;

public class BirthMessageListenerInsert extends Thread implements MessageListener {

	private BirthDAO bdao = new BirthDAO();
	static final Logger bMLogger = LogManager.getLogger(BirthMessageListenerInsert.class.getName());
	
	//Aqu� habr�a que insertar
	public void onMessage(Message message) {
		try {
			//Se supone que este es el JSON
			String messageStr = ((TextMessage)message).getText();
			Birth bAux = new JSONDeserializer<Birth>().deserialize(messageStr);
			if(bdao.insertBirth(bAux)){
				//System.out.println("Birth with ID = " + bAux.getBirthid() + " inserted correctly.");
				bMLogger.info("Birth with ID = " + bAux.getBirthid() + " inserted correctly.");
			}
			else{
				bMLogger.error("Fail when trying to insert birth with ID = " + bAux.getBirthid());
				//System.out.println("Fail when trying to insert birth with ID = " + bAux.getBirthid());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	

}
