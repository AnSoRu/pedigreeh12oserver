package topicsMessageListenersThreads;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Union;
import flexjson.JSONDeserializer;
import model.UnionDAO;

public class UnionMessageListenerUpdate extends Thread implements MessageListener {

	private UnionDAO uDao = new UnionDAO();
	
	static final Logger uMLogger = LogManager.getLogger(UnionMessageListenerUpdate.class.getName());

	public void onMessage(Message mess) {
		
		try {
			String messageStr = ((TextMessage)mess).getText();
			Union uAux = new JSONDeserializer<Union>().deserialize(messageStr);
			uMLogger.info("Trying to update union with ID = " + uAux.getUnionId());
			uDao.updateUnion(uAux,new Date());
			uMLogger.info("Union with ID = " + uAux.getUnionId() + " updated correctly.");			
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
