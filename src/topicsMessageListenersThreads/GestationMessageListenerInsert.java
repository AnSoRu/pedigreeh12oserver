package topicsMessageListenersThreads;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Gestation;
import flexjson.JSONDeserializer;
import model.GestationDAO;

public class GestationMessageListenerInsert extends Thread implements MessageListener {

	private GestationDAO gDao = new GestationDAO();
	static final Logger gMLogger = LogManager.getLogger(GestationMessageListenerInsert.class.getName());

	
	public void onMessage(Message message) {
		
		try {
			String messageStr = ((TextMessage)message).getText();
			Gestation gAux = new JSONDeserializer<Gestation>().deserialize(messageStr);
			if(gDao.insertGestation(gAux)){
				gMLogger.info("Gestation with ID = " + gAux.getIdGestation() + " inserted correctly.");
			}
			else{
				gMLogger.info("Fail when trying to insert gestation with ID = " + gAux.getIdGestation());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
