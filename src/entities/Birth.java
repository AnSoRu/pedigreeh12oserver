package entities;
// Generated 01-mar-2016 11:38:17 by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Birth generated by hbm2java
 */
@Entity
@Table(name = "Birth", catalog = "bbddclinica")
public class Birth implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6250723115472068265L;
	private int birthid;
	private Gestation gestation;
	private Date birthday;
	private String type;
	private Date lastModification;
	private Set<Person> persons = new HashSet<Person>(0);

	public Birth() {
	}

	public Birth(int birthid, Date lastModification) {
		this.birthid = birthid;
		this.lastModification = lastModification;
	}

	public Birth(int birthid, Gestation gestation, Date birthday, String type, Date lastModification,
			Set<Person> persons) {
		this.birthid = birthid;
		this.gestation = gestation;
		this.birthday = birthday;
		this.type = type;
		this.lastModification = lastModification;
		this.persons = persons;
	}

	@Id

	@Column(name = "birthid", unique = true, nullable = false)
	public int getBirthid() {
		return this.birthid;
	}

	public void setBirthid(int birthid) {
		this.birthid = birthid;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Gestation_idGestation")
	public Gestation getGestation() {
		return this.gestation;
	}

	public void setGestation(Gestation gestation) {
		this.gestation = gestation;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday", length = 0)
	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "type", length = 45)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastModification", nullable = false, length = 0)
	public Date getLastModification() {
		return this.lastModification;
	}

	public void setLastModification(Date lastModification) {
		this.lastModification = lastModification;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "birth")
	public Set<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

}
