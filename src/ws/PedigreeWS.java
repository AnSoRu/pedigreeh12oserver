package ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import entities.Birth;
import entities.Diagnosis;
import entities.Disorder;
import entities.Gestation;
import entities.Person;
import entities.Union;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import model.BirthDAO;
import model.DiagnosisDAO;
import model.DisorderDAO;
import model.GestationDAO;
import model.PersonDAO;
import model.UnionDAO;
import modelPubSubs.BirthPubSubDAO;
import modelPubSubs.DiagnosisPubSubDAO;
import modelPubSubs.DisorderPubSubDAO;
import modelPubSubs.GestationPubSubDAO;
import modelPubSubs.PersonPubSubDAO;
import modelPubSubs.UnionPubSubDAO;

public class PedigreeWS {
	
	
	static final Logger appLogger = LogManager.getLogger(PedigreeWS.class.getName());

	
	@SuppressWarnings("unused")
	private static BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
	@SuppressWarnings("unused")
	private static DiagnosisPubSubDAO diagnosisPSDao = new DiagnosisPubSubDAO();
	@SuppressWarnings("unused")
	private static DisorderPubSubDAO disorderPSDao = new DisorderPubSubDAO();
	@SuppressWarnings("unused")
	private static GestationPubSubDAO gestationPSDao = new GestationPubSubDAO();
	@SuppressWarnings("unused")
	private static PersonPubSubDAO personPSDao = new PersonPubSubDAO();
	@SuppressWarnings("unused")
	private static UnionPubSubDAO unionPSDao = new UnionPubSubDAO();
	
	
	/*
	@SuppressWarnings("unused")
	private  BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
	@SuppressWarnings("unused")
	private  DiagnosisPubSubDAO diagnosisPSDao = new DiagnosisPubSubDAO();
	@SuppressWarnings("unused")
	private  DisorderPubSubDAO disorderPSDao = new DisorderPubSubDAO();
	@SuppressWarnings("unused")
	private  GestationPubSubDAO gestationPSDao = new GestationPubSubDAO();
	@SuppressWarnings("unused")
	private  PersonPubSubDAO personPSDao = new PersonPubSubDAO();
	@SuppressWarnings("unused")
	private  UnionPubSubDAO unionPSDao = new UnionPubSubDAO();
	*/

	
	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE BIRTH
	//////////////////////////////////////////////////////////////////////////////////
	public List<Birth>findAllBirths(){
		appLogger.info("Entering findAllBirths.");
		BirthDAO bd = new BirthDAO();
		List<Birth> aux = bd.findAllBirths();
		appLogger.info("Exiting findAllBirths.");
		return aux;
	}	
	public String findAllBirthsJSON(){
		appLogger.info("Entering findAllBirthsJSON");
		BirthDAO bd = new BirthDAO();
		List<Birth> aux = bd.findAllBirths();
		appLogger.info("Exiting findAllBirthsJSON.");
		return new JSONSerializer().serialize(aux);
	}
	//Se supone que aqu� tengo que arrancar el subscriptor y hacer un publish
	private void insertBirth(Birth b){
		appLogger.info("Entering insertBirth.");
		appLogger.info("Trying to insert (publish) birth with ID = " + b.getBirthid() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.birth";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertBirth = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertBirth);
			String bJSON = new JSONSerializer().serialize(b);
			TextMessage textMessage = topicSession.createTextMessage(bJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Birth with ID = " + b.getBirthid() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}
	}

	public boolean insertBirthJSON(String b){
		appLogger.info("Entering insertBirthJSON.");
		boolean res = false;
		Birth bAux = new JSONDeserializer<Birth>().deserialize(b);
		String bAux2 = new JSONSerializer().exclude("lastModification").serialize(bAux);
		BirthDAO bd = new BirthDAO();
		List<Birth> bL1 = bd.getBirthCriteria(bAux.getBirthid());
		if(!bL1.isEmpty()){
			appLogger.info("Exiting insertBirthJSON.");
			return false;
		}
		appLogger.info("Trying to insert (publish) birth with ID = " + bAux.getBirthid() + " into the queue."); 
		insertBirth(bAux);
		//Hacer un getBirth del mismo ID y comparar si son iguales
		List<Birth> bL = bd.getBirthCriteria(bAux.getBirthid());
		while(bL.isEmpty()){
			bL = bd.getBirthCriteria(bAux.getBirthid());
		}
		if(!bL.isEmpty()){
			Birth bLAux = bL.get(0);
			String obtenido = new JSONSerializer().exclude("lastModification").serialize(bLAux);
			if(obtenido.equals(bAux2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertBirthJSON.");
		return res;
	}

	public String getBirthByIDJSON(int birthId){
		appLogger.info("Entering getBirthByIDJSON.");
		appLogger.info("Trying to obtain birth with ID = " + birthId);
		BirthDAO bd = new BirthDAO();
		List<Birth> bAux = bd.getBirthCriteria(birthId);
		appLogger.info("Exiting getBirthByIDJSON.");
		return new JSONSerializer().serialize(bAux);
	}

	private void updateBirth(Birth b){
		appLogger.info("Entering updateBirth.");
		appLogger.info("Trying to update birth with ID = " + b.getBirthid() + " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.birth";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdateBirth = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdateBirth);
			String bJSON = new JSONSerializer().serialize(b);
			TextMessage textMessage = topicSession.createTextMessage(bJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Birth with ID = " + b.getBirthid() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public void updateBirthJSON(String birth){
		appLogger.info("Entering updateBirthJSON.");
		Birth bAux = new JSONDeserializer<Birth>().deserialize(birth);
		updateBirth(bAux);
	}

	public boolean deleteBirthByID(int birthID){
		appLogger.info("Entering deleteBirthByIDJSON.");
		BirthDAO bd = new BirthDAO();
		appLogger.info("Exiting deleteBirthByIDJSON.");
		return bd.deleteBirthByID(birthID);
	}

	private List<Birth> findAllBirthsFromUnion(Union u){
		appLogger.info("Entering findAllBirthsFromUnion.");
		appLogger.info("Trying to obtain all births from union with ID = " + u.getUnionId());
		//Encontrar primero las gestaciones de esa union
		GestationDAO gd = new GestationDAO();
		List<Birth> res = new ArrayList<Birth>();
		List<Gestation> gestU = gd.getGestationsFromUnion(u);
		if(!gestU.isEmpty()){
			for(Gestation g: gestU){
				if(!g.getBirths().isEmpty()){
					Set<Birth> aux = g.getBirths();
					for(Birth baux: aux){
						res.add(baux);
					}
				}
			}
		}
		appLogger.info("Exiting findAllBirthsFromUnion.");
		return res;
	}

	public String findAllBirthsFromUnionJSON(String union){
		appLogger.info("Entering findAllBirthsFromUnionJSON.");
		Union uAux = new JSONDeserializer<Union>().deserialize(union);
		appLogger.info("Trying to obtain all births from union with ID = " + uAux.getUnionId());
		List<Birth> bAux = findAllBirthsFromUnion(uAux);
		appLogger.info("Exiting findAllBirthFromUnionJSON.");
		return new JSONSerializer().serialize(bAux);
	}

	private boolean addBirthToGestation(Birth birth, Gestation gest){
		appLogger.info("Entering addBirthToGestation.");
		appLogger.info("Trying to add birth with ID = " + birth.getBirthid() + " to gestation with ID = " + gest.getIdGestation());
		boolean res = false;
		//Comprobar que existe el nacimiento en la bbdd
		BirthDAO bd = new BirthDAO();
		List<Birth> bAux = bd.getBirthCriteria(birth.getBirthid());
		if(bAux.isEmpty()){
			appLogger.info("The birth with ID = " + birth.getBirthid() + " doesn't exist.");
			appLogger.info("Trying to insert birth with ID = " + birth.getBirthid());
			insertBirth(birth);
			while(getBirthByIDJSON(birth.getBirthid())=="[]"){
				 getBirthByIDJSON(birth.getBirthid());
				//appLogger.info("Unable to insert birth with ID = " + birth.getBirthid());
				//appLogger.info("Exiting addBirthToGestation.");
				//return false;
			}			
		}
		//Aqu� ya est� insertado el nacimiento en la bbdd
		//Ver si tiene alguna gestaci�n asociada
		appLogger.info("Trying to obtain the birth with ID = " + birth.getBirthid());
		List<Birth> bAuxL = bd.getBirthCriteria(birth.getBirthid());
		if(!bAuxL.isEmpty()){
			Birth b2 = bAuxL.get(0);
			appLogger.info("Trying to obtain the gestation associated with it.");
			Gestation gestAux = b2.getGestation();
			//Caso en el que no tenga ninguna gestaci�n asociada
			if(gestAux==null){
				GestationDAO gd = new GestationDAO();
				appLogger.info("There is no gestation associated.");
				appLogger.info("Trying to obtain the gestation with ID = " + gest.getIdGestation());
				List<Gestation> gAux = gd.getGestationCriteria(gest.getIdGestation());
				if(!gAux.isEmpty()){
					//La gestacion ya existe
					//�Estan asociados?
					Gestation gestEx = gAux.get(0);
					//�Est� el nacimiento en la gestaci�n?
					//Obtengo los nacimientos de la gestaci�n
					Set<Birth> nacGest = gestEx.getBirths();
					appLogger.info("Trying to obtain all births associated with the gestation with ID = " + gest.getIdGestation());
					if(!nacGest.isEmpty()){
						//La gestaci�n ya tiene nacimientos
						//Ver si entre ellos esta el que quiero a�adir
						appLogger.info("Checking if the birth with ID = " + birth.getBirthid() + " is already associated with the gestation.");
						boolean existe = false;
						for(Birth bAux2 : nacGest){
							if(bAux2.getBirthid()==birth.getBirthid()){
								res = false;
								existe = true;
								break;
							}
						}
						//Caso en el que ya exista el nacimiento en esa gestaci�n
						//En principio no deber�a de darse este caso porque
						//Si esto y en esta parte es porque el nacimiento no tiene 
						//asociada ninguna gestaci�n.
						if(existe){
							appLogger.error("The birth with ID = " + birth.getBirthid() + " is already associated.");
							res = false;
						}
						else{
							appLogger.info("The birth with ID = " + birth.getBirthid() + " is not associated.");
							appLogger.info("Adding the birth with ID = " + birth.getBirthid() + " to the gestation with ID = " + gest.getIdGestation());
							nacGest.add(b2);
							gestEx.setBirths(nacGest);
							//gd.updateGestation(gestEx);
							updateGestation(gestEx);
							b2.setGestation(gestEx);
							//bd.updateBirth(b2);
							updateBirth(b2);
							appLogger.info("Birth with ID = " + birth.getBirthid() + " added correctly to the gestation with ID = " + gest.getIdGestation());
							res = true;
						}
					}
					//No hay nacimientos en esa gestaci�n
					else{
						appLogger.info("There are no births associated with the gestation with ID = " + gest.getIdGestation());
						appLogger.info("Adding the birth with ID = " + birth.getBirthid() + " to the gestation with ID = " + gest.getIdGestation());
						nacGest.add(b2);
						gestEx.setBirths(nacGest);
						//gd.updateGestation(gestEx);
						updateGestation(gestEx);
						b2.setGestation(gestEx);
						//bd.updateBirth(b2);
						updateBirth(b2);
						appLogger.info("Birth with ID = " + birth.getBirthid() + " added correctly to the gestation with ID = " + gest.getIdGestation());
						res = true;
					}
				}else{
					appLogger.info("The gestation with ID = " + gest.getIdGestation() + " doesn't exist.");
					appLogger.info("Trying to insert gestation with ID = " + gest.getIdGestation());
					Set<Birth> nacAux = new HashSet<Birth>();
					nacAux.add(b2);
					gest.setBirths(nacAux);
					insertGestation(gest);
					List<Gestation> gLAux = gd.getGestationCriteria(gest.getIdGestation());
					if(gLAux.isEmpty()){
						//if(!gd.insertGestation(gest)){
						appLogger.error("Unable to insert gestation with ID = " + gest.getIdGestation());
						appLogger.info("Exiting addBirthToGestation.");
						return false;
					}
					else{
						appLogger.info("Gestation with ID = " + gest.getIdGestation() + " inserted correctly.");
						b2.setGestation(gestAux);
						appLogger.info("Adding the gestation with ID = " + gest.getIdGestation() + " to the birth with ID = " + birth.getBirthid());
						//bd.updateBirth(b2,new Date());
						b2.setLastModification(new Date());
						updateBirth(b2);
						res = true;
					}
				}
			}
			//Ya tiene una gestaci�n asociada
			else{
				appLogger.info("The birth with ID = " + birth.getBirthid() + " has already a gestation associated with it.");
				appLogger.info("Checking if both gestations are the same.");
				if(gestAux.getIdGestation() == gest.getIdGestation()){
					appLogger.info("Both gestations are the same.");
					res = true;
				}
				//La gestaci�n que tiene asociada y la que le paso no son la misma.
				else{
					appLogger.error("The gestations are not the same.");
					appLogger.error("Fail to associate the birth with ID = " + birth.getBirthid() + " to the gestation with ID = " + gest.getIdGestation());
					res = false;
				}
			}
		}	
		appLogger.info("Exiting addBirthToGestation");
		return res;
	}

	public boolean addBirthToGestationJSON(String birth, String gest){
		appLogger.info("Entering addBirthToGestationJSON.");
		Birth bAux = new JSONDeserializer<Birth>().deserialize(birth);
		Gestation gAux = new JSONDeserializer<Gestation>().deserialize(gest);
		boolean res = addBirthToGestation(bAux,gAux);
		appLogger.info("Exiting addBirthToGestationJSON.");
		return res;
	}	
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE BIRTH
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE DIAGNOSIS
	//////////////////////////////////////////////////////////////////////////////////
	public List<Diagnosis> findAllDiagnoses(){
		appLogger.info("Entering findAlldiagnoses.");
		DiagnosisDAO dd = new DiagnosisDAO();
		appLogger.info("Exiting findAllDiagnoses.");
		return dd.findAllDiagnosises();
	}

	public String findAllDiagnosesJSON(){
		appLogger.info("Entering findAllDiagnosesJSON.");
		DiagnosisDAO dd = new DiagnosisDAO();
		List<Diagnosis> res = dd.findAllDiagnosises();
		appLogger.info("Exiting findAllDiagnosesJSON.");
		return new JSONSerializer().serialize(res);		
	}

	private void insertDiagnosis(Diagnosis d){
		appLogger.info("Entering insertDiagnosis.");
		appLogger.info("Trying to insert (publish) diagnosis with ID = " + d.getIdDiagnosis() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.diagnosis";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertDiagnosis = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertDiagnosis);
			String dJSON = new JSONSerializer().serialize(d);
			TextMessage textMessage = topicSession.createTextMessage(dJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Diagnosis with ID = " + d.getIdDiagnosis() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public boolean insertDiagnosisJSON(String diag){
		appLogger.info("Entering insertDiagnosisJSON.");
		boolean res = false;
		Diagnosis dAux = new JSONDeserializer<Diagnosis>().deserialize(diag);
		String dAux2 = new JSONSerializer().exclude("lastModification").serialize(dAux);
		DiagnosisDAO dd = new DiagnosisDAO();
		List<Diagnosis> dL1 = dd.getDiagnosisCriteria(dAux.getIdDiagnosis());
		if(!dL1.isEmpty()){
			appLogger.info("Exiting insertDiagnosisJSON.");
			return false;
		}
		appLogger.info("Trying to insert diagnosis with ID = " + dAux.getIdDiagnosis());
		insertDiagnosis(dAux);
		List<Diagnosis> dL = dd.getDiagnosisCriteria(dAux.getIdDiagnosis());
		while(dL.isEmpty()){
			dL = dd.getDiagnosisCriteria(dAux.getIdDiagnosis());
		}
		if(!dL.isEmpty()){
			Diagnosis dLAux = dL.get(0);
			String obtenido = new JSONSerializer().exclude("lastModification").serialize(dLAux);
			if(obtenido.equals(dAux2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertDiagnosisJSON.");
		return res;
	}

	public String getDiagnosisByIDJSON(int diagnosisId){
		appLogger.info("Entering getDiagnosisByIDJSON.");
		appLogger.info("Trying to obtain diagnosis with ID = " + diagnosisId);
		DiagnosisDAO dd = new DiagnosisDAO();
		List<Diagnosis> lAux = dd.getDiagnosisCriteria(diagnosisId);
		appLogger.info("Exiting getDiagnosisByIDJSON.");
		return new JSONSerializer().serialize(lAux);
	}

	private void updateDiagnosis(Diagnosis d){
		appLogger.info("Entering updateDiagnosis.");
		appLogger.info("Trying to update diagnosis with ID = " + d.getIdDiagnosis()+ " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.diagnosis";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdateDiagnosis = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdateDiagnosis);
			String dJSON = new JSONSerializer().serialize(d);
			TextMessage textMessage = topicSession.createTextMessage(dJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Diagnosis with ID = " + d.getIdDiagnosis() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public void updateDiagnosisJSON(String diag){
		appLogger.info("Entering updateDiagnosisJSON.");
		Diagnosis dAux = new JSONDeserializer<Diagnosis>().deserialize(diag);
		appLogger.info("Trying to update diagnosis with ID = " + dAux.getIdDiagnosis());
		updateDiagnosis(dAux);
		appLogger.info("Exiting updateDiagnosisJSON.");
	}

	public boolean deleteDiagnosisByID(int diagnosisId){
		appLogger.info("Entering deleteDiagnosisByIDJSON.");
		appLogger.info("Trying to delete diagnosis with ID = " + diagnosisId);
		DiagnosisDAO dd = new DiagnosisDAO();
		appLogger.info("Exiting deleteDiagnosisByIDJSON.");
		return dd.deleteDiagnosisByID(diagnosisId);
	}

	public String findAllDiagnosesForPersonByIDJSON(String personId){
		appLogger.info("Entering findAllDiagnosesForPersonByIDJSON.");
		appLogger.info("Trying to obtain all diagnoses for person with ID = " + personId);
		PersonDAO pd = new PersonDAO();
		List<Person> pAux = pd.getPersonCriteria(personId);
		Set<Diagnosis> diagAux = new HashSet<Diagnosis>();
		List<Diagnosis> dRestAux = new ArrayList<Diagnosis>();
		if(!pAux.isEmpty()){
			Person perAux = pAux.get(0);
			diagAux = perAux.getDiagnosises();
			if(!diagAux.isEmpty()){
				for(Diagnosis d : diagAux){
					dRestAux.add(d);
				}
			}
		}
		appLogger.info("Exiting findAllDiagnosesForPersonByIDJSON.");
		return new JSONSerializer().serialize(dRestAux);		
	}

	public boolean addDisorderCodesToDiagnoJSON(String codes, String diagno){
		appLogger.info("Entering addDisorderCodesToDiagnoJSON.");
		DiagnosisDAO dd = new DiagnosisDAO();
		List<Disorder> lIcd = new JSONDeserializer<List<Disorder>>().deserialize(codes);
		Diagnosis diagnoAux = new JSONDeserializer<Diagnosis>().deserialize(diagno);
		appLogger.info("Trying to add a list of disorder codes to the diagnosis with ID = " + diagnoAux.getIdDiagnosis());
		List<Diagnosis> dAux = dd.getDiagnosisCriteria(diagnoAux.getIdDiagnosis());
		boolean res = false;
		if(!dAux.isEmpty()){
			Set<Disorder> codesSet = new HashSet<Disorder>();
			codesSet.addAll(lIcd);
			Diagnosis d1 = dAux.get(0);
			d1.setDisorders(codesSet);
			updateDiagnosis(d1);
			res = true;
			appLogger.info("List of disorder codes added correctly to the diagnosis with ID = " + diagnoAux.getIdDiagnosis());
		}
		else{
			appLogger.info("Unable to add a list of disorder codes. The diagnosis with ID = " + diagnoAux.getIdDiagnosis() + " doesn't exist.");
			res = false;
		}
		appLogger.info("Exiting addDisorderCodesToDiagnoJSON.");
		return res;
	}

	//1)Al menos la persona tiene que existir previamente en la bbdd
	//El diagn�stico puede existir o no previamente
	//2)Si el diagn�stico existe
	//Comprobar si est� o no asignado
	//1.1) Si EST� ASIGNADO (independientemente de que la persona 
	//pasada como par�metro sea la misma a la que est� asignado-> error
	//NOTA: 
	//1.2) Si no est� asignado -> ASIGNAR A LA PERSONA
	//3)Si el diagn�stico no existe
	//Insertar el diagn�stico
	//Asociarlo a la persona
	private boolean addDiagnosisForPerson(Diagnosis diag, Person p){
		appLogger.info("Entering addDiagnosisForPerson.");
		appLogger.info("Trying to add the diagnosis with ID = " + diag.getIdDiagnosis() + " to the person with ID = " + p.getClinicalhistoryid());
		PersonDAO pd = new PersonDAO();
		appLogger.info("Checking if the person with ID = " + p.getClinicalhistoryid() + " exists in the database.");
		List<Person> pAux = pd.getPersonCriteria(p.getClinicalhistoryid());
		boolean res = false;
		//Comprobar que existe la persona
		if(!pAux.isEmpty()){
			Person persAux = pAux.get(0);
			//Comprobar que el diagnostico previamente no existe
			//Si existe que no est� asociado a nadie
			DiagnosisDAO dd = new DiagnosisDAO();
			appLogger.info("Checking if the diagnosis with ID = " + diag.getIdDiagnosis() + " exists in the database");
			List<Diagnosis> dAux = dd.getDiagnosisCriteria(diag.getIdDiagnosis());
			//Caso en el que exista el diagn�stico en la bbdd
			if(!dAux.isEmpty()){
				Diagnosis diagnoAux = dAux.get(0);
				if(diagnoAux.getPerson()==null){
					Set<Diagnosis> diagPers = persAux.getDiagnosises();
					for(Diagnosis d : diagPers){
						if(d.getIdDiagnosis() == diag.getIdDiagnosis()){
							appLogger.error("The diagnosis with ID = " + diag.getIdDiagnosis() + " is already associated.");
							appLogger.info("Exiting addDiagnosisForPerson");
							return false;
						}
					}
					//Aqui se que no lo tiene asociado
					//Se lo a�ado y lo actualizo
					diagPers.add(diagnoAux);
					updatePerson(persAux);
					//pd.updatePerson(persAux);
					diagnoAux.setPerson(persAux);
					updateDiagnosis(diagnoAux);
					//dd.updateDiagnosis(diagnoAux);
					res = true;
				}
				//Caso en el que ya est� asociado a alguien
				else{
					appLogger.error("The diagnosis with ID = " + diag.getIdDiagnosis() + " is already associated.");
					appLogger.info("Exiting addDiagnosisForPerson.");
				}
			}
			//Caso en el que no exista el diagnostico
			//Asociarlo e insertarlo
			else{
				appLogger.info("The diagnosis with ID = " + diag.getIdDiagnosis() + " doesn't exist.");
				appLogger.info("Trying to insert the diagnosis with ID = " + diag.getIdDiagnosis());
				diag.setPerson(persAux);
				insertDiagnosis(diag);
				List<Diagnosis> diAux = dd.getDiagnosisCriteria(diag.getIdDiagnosis());
				if(diAux.isEmpty()){
					appLogger.error("Failure when inserting the diagnosis with ID = " + diag.getIdDiagnosis());
					appLogger.info("Exiting addDiagnosisForPerson.");
					return false;
				}
				Set<Diagnosis> diagPer = persAux.getDiagnosises();
				diagPer.add(diag);
				//pd.updatePerson(persAux);
				updatePerson(persAux);
				res = true;
			}
		}
		//Caso en el que no exista la persona
		else{
			appLogger.error("The person with ID = " + p.getClinicalhistoryid() + " doesn't exist.");
			appLogger.info("Exiting addDiagnosisForPerson.");
			return false;
		}
		appLogger.info("Exiting addDiagnosisForPerson.");
		return res;
	}

	public boolean addDiagnosisForPersonJSON(String diagno, String person){
		appLogger.info("Entering addDiagnosisForPersonJSON.");
		Diagnosis dAux = new JSONDeserializer<Diagnosis>().deserialize(diagno);
		Person pAux = new JSONDeserializer<Person>().deserialize(person);
		appLogger.info("Trying to add the diagnosis with ID = " + dAux.getIdDiagnosis() + " to the person with ID = " + pAux.getClinicalhistoryid());
		boolean res = addDiagnosisForPerson(dAux, pAux);
		if(res){
			appLogger.info("Diagnosis with ID = " + dAux.getIdDiagnosis() + " added correctly.");
		}
		else{
			appLogger.error("Unable to add diagnosis with ID = " + dAux.getIdDiagnosis() + " to the person with ID = " + pAux.getClinicalhistoryid());
		}
		return res;		
	}
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE DIAGNOSIS
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE DISORDER
	//////////////////////////////////////////////////////////////////////////////////
	public List<Disorder> findAllDisorderCodes(){
		appLogger.info("Entering findAllDisorderCodes.");
		DisorderDAO disorderD = new DisorderDAO();
		appLogger.info("Exing findAllDisorderCodes.");
		return disorderD.findAllDisorderCodes();
	}

	public String findAllDisorderCodesJSON(){
		appLogger.info("Entering findAllDisorderCodesJSON.");
		DisorderDAO disorderD = new DisorderDAO();
		List<Disorder> lIcd = disorderD.findAllDisorderCodes();
		appLogger.info("Exiting findDisorderIcdCodesJSON.");
		return new JSONSerializer().serialize(lIcd);
	}

	private void insertDisorder(Disorder dis){
		appLogger.info("Entering insertDisorder.");
		appLogger.info("Trying to insert (publish) disorder with ID = " + dis.getDisorderCode() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.disorder";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertDisorder = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertDisorder);
			String disJSON = new JSONSerializer().serialize(dis);
			TextMessage textMessage = topicSession.createTextMessage(disJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Disorder with ID = " + dis.getDisorderCode() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	//ATENCI�N: el par�metro es una cadena JSON que representa a un objeto de tipo Icdcode
	//ATENCI�N: el par�metro NO ES el propio codigo ES UN OBJETO
	public boolean insertDisorderCodeJSON(String disorder){
		appLogger.info("Entering insertDisorderCodeJSON.");
		boolean res = false;
		Disorder dCode = new JSONDeserializer<Disorder>().deserialize(disorder);
		String dCode2 = new JSONSerializer().exclude("lastModification").serialize(dCode);
		DisorderDAO dd = new DisorderDAO();
		List<Disorder> dL1 = dd.getDisorderCodeCriteria(dCode);
		if(!dL1.isEmpty()){
			appLogger.info("Exiting insertDisorderCodeJSON.");
			return false;
		}
		appLogger.info("Trying to insert icdCode with ID = " + dCode.getDisorderCode() );
		insertDisorder(dCode);
		List<Disorder> dL = dd.getDisorderCodeCriteria(dCode);
		while(dL.isEmpty()){
			dL = dd.getDisorderCodeCriteria(dCode);
		}
		if(!dL.isEmpty()){
			Disorder dLAux = dL.get(0);
			String obtenido = new JSONSerializer().serialize(dLAux);
			if(obtenido.equals(dCode2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertDisorderCodeJSON.");
		return res;
	}

	//ATENCI�N: el par�metro es una cadena JSON que representa a un objeto de tipo Icdcode
	//ATENCI�N: el par�metro NO ES el propio codigo ES UN OBJETO
	public String getDisorderCodeByIDJSON(String code){
		appLogger.info("Entering getDisorderCodeByIDJSON.");
		Disorder disorderCode = new JSONDeserializer<Disorder>().deserialize(code);
		appLogger.info("Trying to obtain disorder code with ID = " + disorderCode.getDisorderCode());
		DisorderDAO disorderD = new DisorderDAO();
		List<Disorder> lAux = disorderD.getDisorderCodeCriteria(disorderCode);
		appLogger.info("Exiting getDisorderCodeByIDJSON.");
		return new JSONSerializer().serialize(lAux);
	}

	private void updateDisorder(Disorder dis){
		appLogger.info("Entering updateDisorder.");
		appLogger.info("Trying to update disorder with ID = " + dis.getDisorderCode() + " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.disorder";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdateDisorder = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdateDisorder);
			String disJSON = new JSONSerializer().serialize(dis);
			TextMessage textMessage = topicSession.createTextMessage(disJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Disorder with ID = " + dis.getDisorderCode() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	//ATENCI�N: el par�metro es una cadena JSON que representa a un objeto de tipo Icdcode
	//ATENCI�N: el par�metro NO ES el propio codigo ES UN OBJETO
	public void updateDisorderCodeJSON(String code){
		appLogger.info("Entering updateDisorderCodeJSON.");
		Disorder disorderCode = new JSONDeserializer<Disorder>().deserialize(code);
		appLogger.info("Trying to update code with ID = " + disorderCode.getDisorderCode());
		updateDisorder(disorderCode);
		appLogger.info("Exiting updateDisorderCodeJSON.");
	}

	//ATENCI�N: el par�metro es una cadena JSON que representa a un objeto de tipo Icdcode
	//ATENCI�N: el par�metro NO ES el propio codigo ES UN OBJETO
	public boolean deleteDisorderCodeByIDJSON(String code){
		appLogger.info("Entering deleteDisorderCodeJSON.");
		Disorder disorderCode = new JSONDeserializer<Disorder>().deserialize(code);
		DisorderDAO dd = new DisorderDAO();
		appLogger.info("Trying to delete code with ID = " + disorderCode.getDisorderCode());
		appLogger.info("Exiting deleteDisorderCodeJSON.");
		return dd.deleteIcdCodeByID(disorderCode.getDisorderCode());
	}
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE DISORDER
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE GESTATION
	//////////////////////////////////////////////////////////////////////////////////

	public List<Gestation> findAllGestations(){
		appLogger.info("Entering findAllGestations.");
		GestationDAO gd = new GestationDAO();
		appLogger.info("Exiting findAllGestations.");
		return gd.findAllGestations();
	}

	public String findAllGestationsJSON(){
		appLogger.info("Entering findAllGestationsJSON.");
		GestationDAO gd = new GestationDAO();
		List<Gestation> res = gd.findAllGestations();
		appLogger.info("Exiting findAllGestationsJSON.");
		return new JSONSerializer().serialize(res);
	}

	private void insertGestation(Gestation gest){
		appLogger.info("Entering insertGestation.");
		appLogger.info("Trying to insert (publish) gestation with ID = " + gest.getIdGestation() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.gestation";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertGestation = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertGestation);
			String gJSON = new JSONSerializer().serialize(gest);
			TextMessage textMessage = topicSession.createTextMessage(gJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Gestation with ID = " + gest.getIdGestation() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public boolean insertGestationJSON(String gest){
		appLogger.info("Entering insertGestationJSON.");
		boolean res = false;
		Gestation gAux = new JSONDeserializer<Gestation>().deserialize(gest);
		String gAux2 = new JSONSerializer().exclude("lastModification").serialize(gAux);
		GestationDAO gd = new GestationDAO();
		List<Gestation> gL1 = gd.getGestationCriteria(gAux.getIdGestation());
		if(!gL1.isEmpty()){
			appLogger.info("Exiting insertGestationJSON.");
			return false;
		}
		appLogger.info("Trying to insert gestation with ID = " + gAux.getIdGestation());
		insertGestation(gAux);
		List<Gestation> gL = gd.getGestationCriteria(gAux.getIdGestation());
		while(gL.isEmpty()){
			gL = gd.getGestationCriteria(gAux.getIdGestation());
		}
		if(!gL.isEmpty()){
			Gestation gLAux = gL.get(0);
			String obtenido = new JSONSerializer().exclude("lastModification").serialize(gLAux);
			if(obtenido.equals(gAux2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertGestationJSON.");
		return res;
	}

	public String getGestationByIDJSON(int gestId){
		appLogger.info("Entering getGestationByIDJSON.");
		appLogger.info("Trying to obtain gestation with ID = " + gestId);
		GestationDAO gd = new GestationDAO();
		List<Gestation> lAux = gd.getGestationCriteria(gestId);
		appLogger.info("Exiting getGestationByIDJSON.");
		return new JSONSerializer().serialize(lAux);		
	}

	private void updateGestation(Gestation gest){
		appLogger.info("Entering updateGestation.");
		appLogger.info("Trying to update gestation with ID = " + gest.getIdGestation() + " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.gestation";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdateGestation = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdateGestation);
			String gJSON = new JSONSerializer().serialize(gest);
			TextMessage textMessage = topicSession.createTextMessage(gJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Gestation with ID = " + gest.getIdGestation() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}	
	}

	public void updateGestationJSON(String gest){
		appLogger.info("Entering updateGestationJSON.");
		Gestation gAux = new JSONDeserializer<Gestation>().deserialize(gest);
		appLogger.info("Trying to update gestation with ID = " + gAux.getIdGestation());
		updateGestation(gAux);
		appLogger.info("Exiting updateGestationJSON.");
	}

	public boolean deleteGestationByID(int gestId){
		appLogger.info("Entering deleteGestationByID.");
		GestationDAO gd = new GestationDAO();
		appLogger.info("Trying to delete gestation with ID = " + gestId);
		appLogger.info("Exiting deleteGestationByID.");
		return gd.deleteGestationByID(gestId);		
	}

	public String findAllGestationsFromUnionJSON(String union){
		appLogger.info("Entering findAllGestationsFromUnionJSON.");
		Union uAux = new JSONDeserializer<Union>().deserialize(union);
		appLogger.info("Trying to find all gestations from union with ID = " + uAux.getUnionId());
		GestationDAO gd = new GestationDAO();
		List<Gestation> gestU = gd.getGestationsFromUnion(uAux);
		appLogger.info("Exiting findAllGestationsFromUnionJSON.");
		return new JSONSerializer().serialize(gestU);
	}

	private boolean addGestationToUnion(Gestation gest, Union u){
		appLogger.info("Entering addGestationToUnion.");
		appLogger.info("Trying to add gestation with ID = " + gest.getIdGestation() + " to the union with ID = " + u.getUnionId());
		UnionDAO uDao = new UnionDAO();
		boolean res = false;
		//�Existe la union?
		appLogger.info("Checking if the union with ID = " + u.getUnionId() + " exists.");
		List<Union> uAuxL = uDao.getUnionCriteria(u.getUnionId());
		if(uAuxL.isEmpty()){
			appLogger.error("The union with ID = " + u.getUnionId() + " doesn't exist.");
			appLogger.info("Exiting addGestationToUnion.");
			return false;
		}
		Union uAux = uAuxL.get(0);
		//�Existe la gestaci�n?
		//Comprobar si existe la gestaci�n
		GestationDAO gd = new GestationDAO();
		appLogger.info("Checking if the gestation with ID = " + gest.getIdGestation() + " exists.");
		List<Gestation> gAuxL = gd.getGestationCriteria(gest.getIdGestation());
		//Caso en el que no exista la gestaci�n
		if(gAuxL.isEmpty()){
			appLogger.info("The gestation with ID = " + gest.getIdGestation() + " doesn't exist.");
			appLogger.info("Trying to insert gestation with ID = " + gest.getIdGestation());
			//Insertar la gestaci�n
			gest.setUnion(uAux);
			insertGestation(gest);
			List<Gestation> lAux = gd.getGestationCriteria(gest.getIdGestation());
			while(lAux.isEmpty()){
				//if(!gd.insertGestation(gest)){
				lAux = gd.getGestationCriteria(gest.getIdGestation());
				//appLogger.error("Cannot insert gestation with ID = " + gest.getIdGestation());
				//appLogger.info("Exiting addGestationToUnion.");
				//return false;
			}
			res = true;
		}
		else{
			appLogger.info("The gestation with ID = " + gest.getIdGestation() + " exists.");
			//Aqu� ya existe la gestaci�n
			List<Gestation> gAuxL2 = gd.getGestationCriteria(gest.getIdGestation());
			appLogger.info("Checking if the gestation with ID = " + gest.getIdGestation() + " is already associated.");
			//�Est� asociada?
			Gestation gestAux = gAuxL2.get(0);
			//Caso en el que ya est� asociada
			if(gestAux.getUnion()!=null){
				appLogger.info("The gestation with ID = " + gest.getIdGestation() + " is already associated.");
				//�Es la misma uni�n?
				//Caso en el que sea la misma uni�n
				if(gestAux.getUnion().getUnionId() == uAux.getUnionId()){
					appLogger.info("The gestation with ID = " + gest.getIdGestation() + " is associated to the same union ( " + u.getUnionId() + " )");
					appLogger.info("Exiting addGestationToUnion.");
					return true;
				}
				else{
					appLogger.info("The gestation with ID = " + gest.getIdGestation() + " is associated to another union.");
					appLogger.info("Exiting addGestationToUnion.");
					return true;
				}
			}
			//Caso en el que no este asociada
			else{
				appLogger.info("The gestation with ID = " + gest.getIdGestation() + " is not yet associated.");
				appLogger.info("Associating the gestation with ID = " + gest.getIdGestation() + " to the union with ID = " + u.getUnionId());
				//le asocio la union y la actualizo
				gestAux.setUnion(uAux);
				//gd.updateGestation(gestAux);
				updateGestation(gestAux);
				Set<Gestation> gestationsUnion = uAux.getGestations();
				gestationsUnion.add(gestAux);
				//uDao.updateUnion(uAux);
				updateUnion(uAux);
				res = true;			
			}
		}
		appLogger.info("Exiting addGestationToUnion.");
		return res;
	}

	public boolean addGestationToUnionJSON(String gest,String union){
		appLogger.info("Entering addGestationToUnionJSON.");
		Gestation gestAux = new JSONDeserializer<Gestation>().deserialize(gest);
		Union unAux = new JSONDeserializer<Union>().deserialize(union);
		appLogger.info("Trying to add gestation with ID = " + gestAux.getIdGestation() + " to the union with ID = " + unAux.getUnionId());
		return addGestationToUnion(gestAux, unAux);
	}
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE GESTATION
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE PERSON
	//////////////////////////////////////////////////////////////////////////////////

	public List<Person> findAllPersons(){
		appLogger.info("Entering findAllPersons.");
		PersonDAO pd = new PersonDAO();
		appLogger.info("Exiting findAllPersons.");
		return pd.findAllPersons();
	}

	public String findAllPersonsJSON(){
		appLogger.info("Entering findAllPersonsJSON.");
		PersonDAO pd = new PersonDAO();
		List<Person> aux = pd.findAllPersons();
		appLogger.info("Exiting findAllPersonsJSON.");
		return new JSONSerializer().serialize(aux);
	}

	public Person[] findAllPersonsArray(){
		appLogger.info("Entering findAllPersons.");
		PersonDAO pDao = new PersonDAO();
		appLogger.info("Exiting findAllPersons.");
		List<Person> aux = pDao.findAllPersons();
		Person [] res = new Person[aux.size()];
		for(int i = 0 ; i < res.length ; i++){
			res[i] = aux.get(i);
		}
		return res;
	}

	private void insertPerson(Person per){
		appLogger.info("Entering insertPerson.");
		appLogger.info("Trying to insert (publish) person with ID = " + per.getClinicalhistoryid() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.person";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertPerson = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertPerson);
			String pJSON = new JSONSerializer().serialize(per);
			TextMessage textMessage = topicSession.createTextMessage(pJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Person with ID = " + per.getClinicalhistoryid() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public boolean insertPersonJSON(String p){
		appLogger.info("Entering insertPersonJSON.");
		boolean res = false;
		Person pAux = new JSONDeserializer<Person>().deserialize(p);	
		String pAux2 = new JSONSerializer().exclude("lastModification").serialize(pAux);
		PersonDAO pd = new PersonDAO();
		List<Person> pL1 = pd.getPersonCriteria(pAux.getClinicalhistoryid());
		if(!pL1.isEmpty()){
			appLogger.info("Exiting insertPersonJSON.");
			return false;
		}
		appLogger.info("Trying to insert person with ID = " + pAux.getClinicalhistoryid());
		insertPerson(pAux);
		List<Person> pL = pd.getPersonCriteria(pAux.getClinicalhistoryid());
		while(pL.isEmpty()){
			pL = pd.getPersonCriteria(pAux.getClinicalhistoryid());
		}
		if(!pL.isEmpty()){
			Person pLAux = pL.get(0);
			String obtenido = new JSONSerializer().exclude("lastModification").serialize(pLAux);
			if(obtenido.equals(pAux2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertPersonJSON.");
		return res;
	}

	public String getPersonByIDJSON(String id){
		appLogger.info("Entering getPersonByIDJSON.");
		appLogger.info("Trying to obtain person with ID = " + id);
		PersonDAO pd = new PersonDAO();
		List<Person> auxL = pd.getPersonCriteria(id);
		Person pAux = new Person();
		if(!auxL.isEmpty()){
			pAux = auxL.get(0);
		}
		return new JSONSerializer().serialize(pAux);
	}	

	private void updatePerson(Person per){
		appLogger.info("Entering updatePerson.");
		appLogger.info("Trying to update person with ID = " + per.getClinicalhistoryid() + " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.person";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdatePerson = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdatePerson);
			String pJSON = new JSONSerializer().serialize(per);
			TextMessage textMessage = topicSession.createTextMessage(pJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Person with ID = " + per.getClinicalhistoryid() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public void updatePesonJSON(String per){
		appLogger.info("Entering updatePersonJSON.");
		Person paux = new JSONDeserializer<Person>().deserialize(per);
		appLogger.info("Trying to update person with ID = " + paux.getClinicalhistoryid());
		updatePerson(paux);
		appLogger.info("Exiting updatePersonJSON.");
	}

	public boolean deletePersonByID(String personId){
		appLogger.info("Entering deletePersonById");
		appLogger.info("Trying to delete person with ID = " + personId);
		PersonDAO pd = new PersonDAO();
		appLogger.info("Exiting deletePersonByID.");
		return pd.deletePersonByID(personId);
	}

	private List<String> getPatientsByDisorder(List<Disorder> affections){
		appLogger.info("Entering getPatientsByDisorder.");
		//Hacer una criba de c�digos Icds que tengo en la BBDD
		List<String> aux = new ArrayList<String>();
		if(!affections.isEmpty()){
			DisorderDAO disorderDao = new DisorderDAO();
			for(Disorder code : affections){
				List<Disorder> lAux = disorderDao.getDisorderCodeCriteria(code);
				if(!lAux.isEmpty()){
					//El c�digo IcdExiste -> quiere decir que hay diagn�sticos asociados a ese c�digo
					Disorder codeAux = lAux.get(0);
					Set<Diagnosis> dAux = codeAux.getDiagnosises();
					if(!dAux.isEmpty()){
						//Obtener las personas que estan asociadas a ese diagn�stico
						for(Diagnosis d : dAux){
							Person pAux = d.getPerson();
							if(pAux!=null){
								aux.add(pAux.getClinicalhistoryid());
								appLogger.info("Person with ID = " + pAux.getClinicalhistoryid() + " added to the final list.");
							}
						}
					}
				}
				else{
					appLogger.info("The disorder order with ID = " + code.getDisorderCode() + " doesn't exist in the database.");
				}
			}
		}
		else{
			appLogger.error("The list received has no ICD codes.");
		}
		appLogger.info("Exiting getPatientsByDisorder.");
		return aux;
	}

	public String getPatientsByDisorderJSON(String affections){
		appLogger.info("Entering getPatientsByDisorder");
		List<Disorder> dAux = new JSONDeserializer<List<Disorder>>().deserialize(affections);
		List<String> idsPatients = getPatientsByDisorder(dAux);
		appLogger.info("Exiting getPatientsByDisorderJSON");
		return new JSONSerializer().serialize(idsPatients);
	}
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE PERSON
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OPERACIONES DE UNION
	//////////////////////////////////////////////////////////////////////////////////

	public List<Union> findAllUnions(){
		appLogger.info("Entering findAllUnions.");
		UnionDAO ud = new UnionDAO();
		appLogger.info("Exiting findAllUnions.");
		return ud.findAllUnions();
	}

	public String findAllUnionsJSON(){
		appLogger.info("Entering findAllUnionsJSON.");
		UnionDAO ud = new UnionDAO();
		List<Union> res = ud.findAllUnions();
		appLogger.info("Exiting findAllUnionsJSON.");
		return new JSONSerializer().serialize(res);
	}	

	private void insertUnion(Union un){
		appLogger.info("Entering insertUnion.");
		appLogger.info("Trying to insert (publish) union with ID = " + un.getUnionId() + " into the queue.");
		//Se supone que al crear este nuevo objeto ya se crea y arranca el subscriptor
		//BirthPubSubDAO birthPSDao = new BirthPubSubDAO();
		//Aqu� hago un publish
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "insert.union";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicInsertUnion = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicInsertUnion);
			String uJSON = new JSONSerializer().serialize(un);
			TextMessage textMessage = topicSession.createTextMessage(uJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Union with ID = " + un.getUnionId() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public boolean insertUnionJSON(String union){
		appLogger.info("Entering insertUnionJSON.");
		boolean res = false;
		Union uAux = new JSONDeserializer<Union>().deserialize(union);
		String uAux2 = new JSONSerializer().exclude("lastModification").serialize(uAux);
		UnionDAO ud = new UnionDAO();
		List<Union> uL1 = ud.getUnionCriteria(uAux.getUnionId());
		if(!uL1.isEmpty()){
			appLogger.info("Exiting insertUnionJSON.");
			return false;
		}
		appLogger.info("Trying to insert union with ID = " + uAux.getUnionId());
		insertUnion(uAux);
		List<Union> uL = ud.getUnionCriteria(uAux.getUnionId());
		while(uL.isEmpty()){
			uL = ud.getUnionCriteria(uAux.getUnionId());
		}
		if(!uL.isEmpty()){
			Union uLAux = uL.get(0);
			String obtenido = new JSONSerializer().exclude("lastModification").serialize(uLAux);
			if(obtenido.equals(uAux2)){
				res = true;
			}
			else{
				res = false;
			}
		}
		else{
			res = false;
		}
		appLogger.info("Exiting insertUnionJSON.");
		return res;
	}

	public String getUnionByIDJSON(int unionID){
		appLogger.info("Entering getUnionByIDJSON.");
		appLogger.info("Trying to obtain union with ID = " + unionID);
		UnionDAO ud = new UnionDAO();
		List<Union> res = ud.getUnionCriteria(unionID);
		appLogger.info("Exiting getUnionByIDJSON.");
		return new JSONSerializer().exclude("persons.diagnosises").serialize(res);
	}

	private void updateUnion(Union un){
		appLogger.info("Entering updateUnion.");
		appLogger.info("Trying to update union with ID = " + un.getUnionId() + " into the queue");
		Properties properties = new Properties();
		String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
		String connectionString = "tcp://localhost:61616";
		String topicName = "update.union";
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			TopicSession topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			Topic topicUpdateUnion = (Topic) ctx.lookup(topicName);
			TopicPublisher topicPublisher = topicSession.createPublisher(topicUpdateUnion);
			String uJSON = new JSONSerializer().serialize(un);
			TextMessage textMessage = topicSession.createTextMessage(uJSON);
			topicPublisher.publish(textMessage);
			appLogger.info("Union with ID = " + un.getUnionId() + " published.");
			topicPublisher.close();
			topicSession.close();
			topicConnection.close();
		}
		catch (JMSException e) {
			throw new RuntimeException("Error in JMS operations", e);
		} catch (NamingException e) {
			throw new RuntimeException("Error in initial context lookup", e);
		}		
	}

	public void updateUnionJSON(String union){
		appLogger.info("Entering updateUnionJSON.");
		Union uAux = new JSONDeserializer<Union>().deserialize(union);
		appLogger.info("Trying to update union with ID = " + uAux.getUnionId());
		updateUnion(uAux);
		appLogger.info("Exiting updateUnionJSON.");
	}

	public boolean deleteUnionByID(int unionId){
		appLogger.info("Entering deleteUnionByID.");
		appLogger.info("Trying to delete union with ID = " + unionId);
		UnionDAO ud = new UnionDAO();
		appLogger.info("Exiting deleteUnionByID.");
		return ud.deleteUnionByID(unionId);		
	}

	//Recibe el ID de la persona (NO UN JSON)
	public String getUnionsForPersonByIDJSON(String personId){
		appLogger.info("Entering getUnionsForPersonByIDJSON.");
		appLogger.info("Trying to obtain all unions for person with ID = " + personId);
		UnionDAO ud = new UnionDAO();
		List<Union> lAux = ud.findAllUnionsForPersonByID(personId);
		appLogger.info("Exiting getUnionsForPersonByIDJSON.");
		return new JSONSerializer().serialize(lAux);		
	}

	//Recibe los IDs de la personas (NO SON JSONS)
	public String getUnionsBetweenPeopleJSON(String person1 , String person2){
		appLogger.info("Entering getUnionsBetweenPeopleJSON.");
		UnionDAO ud = new UnionDAO();
		List<Union> aux = ud.getUnionForPeople(person1,person2);
		appLogger.info("Exiting getUnionsBetweenPeopleJSON.");
		return new JSONSerializer().serialize(aux);
	}

	//Recibe los IDs de las Personas (NO SON JSONS)
	public boolean addUnionBetweenPeople(String person1, String person2){
		appLogger.info("Entering addUnionBetweenPeople.");
		PersonDAO pd = new PersonDAO();
		appLogger.info("Checking if both persons exist in the database.");
		List<Person> l1 = pd.getPersonCriteria(person1);
		List<Person> l2 = pd.getPersonCriteria(person2);
		if(l1.isEmpty()){
			appLogger.error("Person with ID = " + person1 + " doesn't exist.");
			appLogger.info("Exiting addUnionBetweenPeople.");
			return false;
		}
		if(l2.isEmpty()){
			appLogger.error("Person with ID = " + person2 + " doesn't exist.");
			appLogger.info("Exiting addUnionBetweenPeople.");
			return false;
		}
		UnionDAO ud = new UnionDAO();
		appLogger.info("Checking if the two people are already linked.");
		List<Union> union = ud.getUnionForPeople(person1, person2);
		if(!union.isEmpty()){
			appLogger.error("Unable to add new union between both persons because they are already linked.");
			appLogger.info("Exiting addUnionBetweenPeople.");
			return false;			
		}
		appLogger.info("Adding new union between people.");
		Union uAux = new Union();
		boolean res = false;
		insertUnion(uAux);

		List<Union> uniones = ud.findAllUnions();
		while(uniones == null || uniones.isEmpty()){
			uniones = ud.findAllUnions();
		}
		for(Union u1 : uniones){
			if(u1.getPersons().isEmpty()){
				uAux = u1;
				break;
			}
		}
		Person p1 = l1.get(0);
		Person p2 = l2.get(0);
		Set<Union> unionesP1 = p1.getUnions();
		Set<Union> unionesP2 = p2.getUnions();
		unionesP1.add(uAux);
		unionesP2.add(uAux);
		Set<Person> personasUnion = uAux.getPersons();
		personasUnion.add(p1);
		personasUnion.add(p2);
		uAux.setPersons(personasUnion);
		uAux.setLastModification(new Date());
		ud.updateUnion(uAux, new Date());

		p1.setUnions(unionesP1);
		p2.setUnions(unionesP2);
		p1.setLastModification(new Date());
		//updatePerson(p1);
		pd.updatePerson(p1,new Date());
		p2.setLastModification(new Date());

		pd.updatePerson(p2,new Date());

		appLogger.info("Union with ID = " + uAux.getUnionId() + " inserted correctly.");
		res = true;
		appLogger.info("Exiting addUnionBetweenPeople.");
		return res;
	}
	//Esto es peligroso porque no se va a poder eliminar as� porque s�
	//Si hay una gestaci�n por medio directamente false
	public boolean deleteUnionBetweenPeople(String person1, String person2){
		appLogger.info("Entering deleteUnionBetweenPeople.");
		PersonDAO pd = new PersonDAO();
		boolean res = false;
		appLogger.info("Checking if both persons exist in the database.");
		List<Person> l1 = pd.getPersonCriteria(person1);
		List<Person> l2 = pd.getPersonCriteria(person2);
		if(l1.isEmpty()){
			appLogger.error("Person with ID = " + person1 + " doesn't exist.");
			appLogger.info("Exiting deleteUnionBetweenPeople.");
			return false;
		}
		if(l2.isEmpty()){
			appLogger.error("Person with ID = " + person2 + " doesn't exist.");
			appLogger.info("Exiting deleteUnionBetweenPeople.");
			return false;
		}
		//Comprobar que existe una una uni�n entre ellos
		appLogger.info("Checking if both persons are already linked.");
		UnionDAO ud = new UnionDAO();
		List<Union> union = ud.getUnionForPeople(person1, person2);
		if(!union.isEmpty()){
			Union uAux = union.get(0);
			appLogger.info("Both people are linked through the union with ID = " + uAux.getUnionId());
			Person p1 = l1.get(0);
			Person p2 = l2.get(0);
			Set<Union> uP1 = p1.getUnions();
			Set<Union> uP2 = p2.getUnions();
			for(Union u1 : uP1){
				if(u1.getUnionId() == uAux.getUnionId()){
					appLogger.info("Removing union with ID = " + uAux.getUnionId() + " from person with ID = " + person1);
					uP1.remove(u1);
					break;
				}
			}
			for(Union u2 : uP2){
				if(u2.getUnionId() == uAux.getUnionId()){
					appLogger.info("Removing union with ID = " + uAux.getUnionId() + " from person with ID = " + person2);
					uP2.remove(u2);
					break;
				}
			}
			appLogger.info("Checking if the union has a gestation.");
			GestationDAO gd = new GestationDAO();
			List<Gestation> gestUnion = gd.getGestationsFromUnion(uAux);
			if(!gestUnion.isEmpty()){
				appLogger.error("Unable to delete union between people because at least one gestation is linked to it.");
				appLogger.info("Exiting deleteUnionBetweenPeople.");
				return false;
			}
			else{
				appLogger.info("Trying to delete union with ID = " + uAux.getUnionId());
				ud.deleteUnionByID(uAux.getUnionId());
				appLogger.info("Updating the list of unions of both persons.");
				p1.setUnions(uP1);
				appLogger.info("Updating person with ID = " + p1.getClinicalhistoryid());
				//pd.updatePerson(p1);
				updatePerson(p1);
				appLogger.info("Person with ID = " + p1.getClinicalhistoryid() + " updated correctly.");
				p2.setUnions(uP2);
				appLogger.info("Updating person with ID = " + p2.getClinicalhistoryid());
				//pd.updatePerson(p2);
				updatePerson(p2);
				appLogger.info("Person with ID = " + p2.getClinicalhistoryid() + " updated correctly.");
				res = true;
			}			
		}
		else{
			appLogger.error("Unable to delete union between people because it doesn't exist any union between them.");
			appLogger.info("Exiting deleteUnionBetweenPeople.");
			return false;
		}
		appLogger.info("Exiting deleteUnionBetweenPeople.");
		return res;
	}
	//////////////////////////////////////////////////////////////////////////////////
	//FIN OPERACIONES DE UNION
	//////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////
	//OTRAS OPERACIONES
	//////////////////////////////////////////////////////////////////////////////////
	//1�Lista los padres (si tiene)
	//2�Lista las uniones (si tiene)
	//3�Lista los hijos (si tiene)
	private List<List<Person>> getParentsAndDescendantsForPersonByID(String personId){
		appLogger.info("Entering getParentsAndDescendantsForPersonByID.");
		appLogger.info("Trying to obtain the parents and descendants for person with ID = " + personId);
		appLogger.info("Checking if the person with ID = " + personId + " exists in the database.");
		List<List<Person>> res = new ArrayList<List<Person>>();
		PersonDAO pd = new PersonDAO();
		List<Person> pAuxL = pd.getPersonCriteria(personId);
		if(pAuxL.isEmpty()){
			appLogger.error("The person with ID = " + personId + " doesn't exist in the database.");
			appLogger.info("Exiting getParentsAndDescendantsForPersonByID.");
			return res;
		}
		List<Person> parents = new ArrayList<Person>();
		List<Person> unionPerson = new ArrayList<Person>();
		List<Person> children = new ArrayList<Person>();
		res.add(parents);
		res.add(unionPerson);
		res.add(children);
		Person pAux = pAuxL.get(0);
		appLogger.info("Trying to obtain the birth of the person with ID = " + personId);
		Birth bAux = pAux.getBirth();
		if(bAux!=null){
			appLogger.info("The birth of the person with ID = " + personId + " is known.");
			appLogger.info("Trying to obtain the gestation of the birth with ID = " + bAux.getBirthid());
			Gestation gAux = bAux.getGestation();
			if(gAux != null){
				//Obtener la union a la cual pertenece la gestaci�n
				appLogger.info("Trying to obtain the union of the gestation with ID = " + gAux.getIdGestation());
				Union uAux = gAux.getUnion();
				if(uAux!=null){
					appLogger.info("Trying to obtain the union with ID = " + uAux.getUnionId());
					Set<Person> personsUnionAux = uAux.getPersons();
					if(personsUnionAux != null){
						for(Person p : personsUnionAux){
							parents.add(p);
						}
					}
				}
			}
			appLogger.info("Trying to obtain all unions related with the person with ID = " + personId);
			//Intentar obtener las uniones que tenga esa persona
			Set<Union> unionsPerson = pAux.getUnions();
			if(!unionsPerson.isEmpty()){
				for(Union u : unionsPerson){
					appLogger.info("Trying to obtain all persons which belong to the union with ID = " + u.getUnionId());
					Set<Person> personasUnion = u.getPersons();
					for(Person pAux2 : personasUnion){
						if(pAux2.getClinicalhistoryid()!=pAux.getClinicalhistoryid()){
							unionPerson.add(pAux2);
							break;
						}
					}
					appLogger.info("Trying to obtain all gestations which belong to the union with ID = " + u.getUnionId() );
					Set<Gestation> gestsUnion = u.getGestations();
					if(!gestsUnion.isEmpty()){
						//La union tiene gestaciones
						for(Gestation gAux2 : gestsUnion){
							//De cada gestacion conozco los posibles nacimientos (si existen)
							appLogger.info("The gestation with ID = " + gAux2.getIdGestation() + " belongs to the union with ID = " + u.getUnionId());
							appLogger.info("Trying to obtain all births of the gestation with ID = " + gAux2.getIdGestation());
							Set<Birth> birthsGestation = gAux2.getBirths();
							if(!birthsGestation.isEmpty()){
								for(Birth bAux2 : birthsGestation){
									Set<Person> personsBirth = bAux2.getPersons();
									for(Person pers2Aux : personsBirth){
										children.add(pers2Aux);
										appLogger.info("Person with ID = " + pers2Aux.getClinicalhistoryid() + " added to the children list.");
									}
								}
							}
							else{
								appLogger.info("The gestation with ID = " + gAux2.getIdGestation() + " doesn't have any birth.");
							}
						}
					}
					else{
						appLogger.info("The union with ID = " + u.getUnionId() + " doesn't have any gestation");
					}				
				}
			}
			else{
				appLogger.info("The person with ID = " + personId + " doesn't have any union.");
			}
		}
		else{
			appLogger.info("The birth of the person with ID = " + personId + " is unkown.");
			//No conozco el nacimiento de esa persona
			//Solo conozco entonces a los descendientes (si tiene)
			appLogger.info("Trying to obtain all unions from the person with ID = " + personId);
			Set<Union> unionsPerson = pAux.getUnions();
			if(!unionsPerson.isEmpty()){
				for(Union u : unionsPerson){
					appLogger.info("Trying to obtain all persons which belong to the union with ID = " + u.getUnionId());
					Set<Person> personasUnion = u.getPersons();
					appLogger.info("Adding persons to the final list.");
					for(Person pAux2 : personasUnion){
						if(pAux2.getClinicalhistoryid()!=pAux.getClinicalhistoryid()){
							unionPerson.add(pAux2);
							appLogger.info("Person with ID = " + pAux2.getClinicalhistoryid() + " added to the final list.");
							break;
						}
					}
					appLogger.info("Trying to obtain all gestations from union with ID = " + u.getUnionId());
					Set<Gestation> gestsUnion = u.getGestations();
					if(!gestsUnion.isEmpty()){
						for(Gestation gAux2 : gestsUnion){
							appLogger.info("Trying to obtain all births from the gestation with ID = " + gAux2.getIdGestation());
							Set<Birth> birthsGestations = gAux2.getBirths();
							if(!birthsGestations.isEmpty()){
								for(Birth bAux2 : birthsGestations){
									appLogger.info("Trying to obtain all births from the gestation with ID = " + gAux2.getIdGestation());
									Set<Person> personsBirth = bAux2.getPersons();
									if(!personsBirth.isEmpty()){
										appLogger.info("Adding children to the final list.");
										for(Person persBAux : personsBirth){
											children.add(persBAux);
										}
									}
									else{
										appLogger.info("The birth with ID = " + bAux2.getBirthid() + "doesn't belong to any person.");
									}
								}								
							}
							else{
								appLogger.info("The gestation with ID = " + gAux2.getIdGestation() + " doesn't have any birth.");
							}
						}
					}
					else{
						appLogger.info("The union with ID = " + u.getUnionId() + " doesn't have any gestation.");
					}
				}
			}
			else{
				appLogger.info("The person with ID = " + personId + " doesn't have any union.");
			}			
		}
		appLogger.info("Exiting getParentsAndDescendantsForPersonByID.");
		return res;
	}

	//Recibe el ID de la persona (NO UN JSON)
	public String getParentsAndDescendantsForPersonByIDJSON(String personId){
		appLogger.info("Entering getParentsAndDescendantsForPersonByIDJSON.");
		List<List<Person>> aux = getParentsAndDescendantsForPersonByID(personId);
		appLogger.info("Exiting getgetParentsAndDescendantsForPersonByIDJSON.");
		return new JSONSerializer().serialize(aux);
	} 
}

