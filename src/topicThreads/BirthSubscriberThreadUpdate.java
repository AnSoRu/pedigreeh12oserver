package topicThreads;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import topicsMessageListenersThreads.BirthMessageListenerUpdate;

public class BirthSubscriberThreadUpdate extends Thread {
	
	private String topicName = "update.birth";
	private String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
	private String connectionString = "tcp://localhost:61616";
	private static TopicSession topicSession = null;
	private static TopicSubscriber updateBirthSubscriber = null;

	
	public BirthSubscriberThreadUpdate(){
		
	}
	
	public void run(){
		Properties properties = new Properties();
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic." + topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();
			
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			
			Topic topicUpdateBirth = (Topic) ctx.lookup(topicName);
			topicConnection.start();
			
			updateBirthSubscriber = topicSession.createSubscriber(topicUpdateBirth);
			BirthMessageListenerUpdate messageListenerUpdate = new BirthMessageListenerUpdate();
			updateBirthSubscriber.setMessageListener(messageListenerUpdate);
		}catch(JMSException e){
			e.printStackTrace();
		}catch(NamingException e){
			e.printStackTrace();
		}
	}
}
