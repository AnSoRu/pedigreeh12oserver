package topicThreads;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import topicsMessageListenersThreads.PersonMessageListenerInsert;

public class PersonSubscriberThreadInsert extends Thread {

	private String topicName = "insert.person";
	private String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
	private String connectionString = "tcp://localhost:61616";
	private static TopicSession topicSession = null;
	private static TopicSubscriber insertPersonSubscriber = null;
	
	public PersonSubscriberThreadInsert(){
		
	}
	
	public void run(){
		Properties properties = new Properties();
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic."+ topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();	
			
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);

			Topic topicInsertPerson = (Topic) ctx.lookup(topicName);
			topicConnection.start();

			insertPersonSubscriber = topicSession.createSubscriber(topicInsertPerson);
			PersonMessageListenerInsert messageListenerInsert = new PersonMessageListenerInsert();
			insertPersonSubscriber.setMessageListener(messageListenerInsert);
			
		}catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
}
