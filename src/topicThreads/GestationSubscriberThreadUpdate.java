package topicThreads;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import topicsMessageListenersThreads.GestationMessageListenerUpdate;


public class GestationSubscriberThreadUpdate extends Thread {

	private String topicName = "update.gestation";
	private String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
	private String connectionString = "tcp://localhost:61616";
	private static TopicSession topicSession = null;
	private static TopicSubscriber updateGestationSubscriber = null;
	
	public GestationSubscriberThreadUpdate(){
		
	}
	
	public void run(){
		Properties properties = new Properties();
		TopicConnection topicConnection = null;
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionFactory.QueueConnectionFactory",connectionString);
		properties.put("topic." + topicName,topicName);
		try{
			InitialContext ctx = new InitialContext(properties);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			topicConnection = topicConnectionFactory.createTopicConnection();
			
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			
			Topic topicUpdateGestation = (Topic) ctx.lookup(topicName);
			topicConnection.start();
			
			updateGestationSubscriber = topicSession.createSubscriber(topicUpdateGestation);
			GestationMessageListenerUpdate messageListenerUpdate = new GestationMessageListenerUpdate();
			updateGestationSubscriber.setMessageListener(messageListenerUpdate);
		}catch(JMSException e){
			e.printStackTrace();
		}catch(NamingException e){
			e.printStackTrace();
		}
	}
}
