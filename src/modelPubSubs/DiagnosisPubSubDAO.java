package modelPubSubs;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import topicThreads.DiagnosisSubscriberThreadInsert;
import topicThreads.DiagnosisSubscriberThreadUpdate;

public class DiagnosisPubSubDAO {
	
	private String topicInsertDiagnosisName = "insert.diagnosis";
	private String topicUpdateDiagnosisName = "update.diagnosis";
	private String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
	private String connectionString = "tcp://localhost:61616";
	InitialContext ctx = null;
	TopicConnectionFactory topicConnectionFactory = null;
	TopicConnection topicConnection = null;
	TopicSubscriber insertDiagnosisSubscriber = null;
	TopicSubscriber updateDiagnosisSubscriber = null;
	private DiagnosisSubscriberThreadInsert hiloSubscriberInsert = null;
	private DiagnosisSubscriberThreadUpdate hiloSubscriberUpdate = null;
	@SuppressWarnings("unused")
	private static TopicSession topicSession = null;
	
	public DiagnosisPubSubDAO(){
		Properties properties = new Properties();
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionfactory.QueueConnectionFactory",connectionString);
		properties.put("topic." + topicInsertDiagnosisName,topicInsertDiagnosisName);
		properties.put("topic." + topicUpdateDiagnosisName,topicUpdateDiagnosisName);
		try{
			this.ctx = new InitialContext(properties);
			this.topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			this.topicConnection = topicConnectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			this.hiloSubscriberInsert = new DiagnosisSubscriberThreadInsert();
			this.hiloSubscriberUpdate = new DiagnosisSubscriberThreadUpdate();
			hiloSubscriberInsert.start();
			hiloSubscriberUpdate.start();
		}catch(NamingException e){
			e.printStackTrace();
		}catch(JMSException e){
			e.printStackTrace();
		}
		finally{
			if(topicConnection != null){
				try{
					topicConnection.close();
				}catch(JMSException e){
					e.printStackTrace();
				}
			}
		}
	}

}
