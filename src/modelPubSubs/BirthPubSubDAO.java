package modelPubSubs;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import topicThreads.BirthSubscriberThreadInsert;
import topicThreads.BirthSubscriberThreadUpdate;

public class BirthPubSubDAO {
	//Clase que implementa la funcionalidad de suscriptor
	
	private String topicInsertBirthName = "insert.birth";
	private String topicUpdateBirthName = "update.birth";
	private String initialContextFactory = "org.apache.activemq.jndi.ActiveMQInitialContextFactory";
	private String connectionString = "tcp://localhost:61616";
	InitialContext ctx = null;
	TopicConnectionFactory topicConnectionFactory = null;
	TopicConnection topicConnection = null;
	TopicSubscriber insertBirthSubscriber = null;
	TopicSubscriber updateBirthSubscriber = null;
	private  BirthSubscriberThreadInsert hiloSubscriberInsert = null;
	private  BirthSubscriberThreadUpdate hiloSubscriberUpdate = null;
	@SuppressWarnings("unused")
	private static TopicSession topicSession = null;
	
	public BirthPubSubDAO(){
		Properties properties = new Properties();
		properties.put("java.naming.factory.initial",initialContextFactory);
		properties.put("connectionfactory.QueueConnectionFactory",connectionString);
		properties.put("topic." + topicInsertBirthName, topicInsertBirthName);
		properties.put("topic." + topicUpdateBirthName,topicUpdateBirthName);
		try {
			this.ctx = new InitialContext(properties);
			this.topicConnectionFactory = (TopicConnectionFactory)ctx.lookup("QueueConnectionFactory");
			this.topicConnection = topicConnectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			
			this.hiloSubscriberInsert = new BirthSubscriberThreadInsert();
			this.hiloSubscriberUpdate = new BirthSubscriberThreadUpdate();
			hiloSubscriberInsert.start();
			hiloSubscriberUpdate.start();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		finally{
			if(topicConnection != null){
				try {
					topicConnection.close();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
