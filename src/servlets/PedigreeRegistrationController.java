package servlets;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * Servlet implementation class PedigreeRegistrationController
 */
public class PedigreeRegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PedigreeRegistrationController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String uName = request.getParameter("josso_username");
		String password = request.getParameter("josso_password");
		String password_2 = request.getParameter("josso_password_2");
		if(uName.isEmpty()){
			RequestDispatcher rd = request.getRequestDispatcher("C:\\apache-tomcat-7.0.65\\webapps\\josso\\signon\\usernamePasswordLogin.jsp");
			out.println("<font color=red> Por favor rellene el campo username</font>");
			rd.include(request, response);
		}
		else if(password.isEmpty()){
			RequestDispatcher rd = request.getRequestDispatcher("C:\\apache-tomcat-7.0.65\\webapps\\josso\\signon\\usernamePasswordLogin.jsp");
			out.println("<font color=red> Por favor rellene el campo password</font>");
			rd.include(request, response);
		}
		else if(password_2.isEmpty()){
			RequestDispatcher rd = request.getRequestDispatcher("C:\\apache-tomcat-7.0.65\\webapps\\josso\\signon\\usernamePasswordLogin.jsp");
			out.println("<font color=red> Por favor rellene el campo repita password</font>");
			rd.include(request, response);
		}
		else if(password != password_2){
			RequestDispatcher rd = request.getRequestDispatcher("C:\\apache-tomcat-7.0.65\\webapps\\josso\\signon\\usernamePasswordLogin.jsp");
			out.println("<font color=red>Ambas passwords deben coincidir</font>");
			rd.include(request, response);
		}
		//Comprobar si el usuario ya existe o no
		SAXBuilder builder = new SAXBuilder();
		File xmlFile = new File("C:\\apache-tomcat-7.0.65\\lib\\josso-users.xml");
		
		try {
			Document document = (Document)builder.build(xmlFile);
			Element rootNode = document.getRootElement();
			//Obtengo la etiqueta <users>
			Element users = rootNode.getChild("users");
			//Etiquetas users
			List<Element> usuarios = users.getChildren("user");
			boolean existe = false;
			for(Element e : usuarios){
				if(e.getContent(1).getValue().equals(uName)){
					out.println("<font color=red> El usuario con nombre " + uName + " ya existe</font>");
					existe = true;
					break;
				}
			}
			if(!existe){
				//1) Modificamos el archivo \lib\josso-users.xml
				//Etiqueta <name>
				Element nuevoPropUser31 = new Element("name");
				nuevoPropUser31.setText("user.registrationDate");
				Element nuevoPropUser32 = new Element("value");
				nuevoPropUser32.setText(new Date().toString());
				
				//Etiqueta <property>
				Element property1 = new Element("property");
				property1.addContent(nuevoPropUser31);
				property1.addContent(nuevoPropUser32);
				
				//Etiqueta <properties>
				Element properties = new Element("properties");
				properties.addContent(property1);
				
				//Etiqueta <roles>
				Element rolesUser = new Element("roles");
				rolesUser.addContent("role1");
				
				//Etiqueta <name>
				Element userName = new Element("name");
				userName.addContent(uName);
				
				Element user = new Element("user");
				user.addContent(userName);
				user.addContent(properties);
				user.addContent(rolesUser);
				
				users.addContent(user);
				
				Format forma1 = Format.getPrettyFormat();
				XMLOutputter xmloutputter1 = new XMLOutputter(forma1);
				xmloutputter1.output(document,new FileWriter("C:\\apache-tomcat-7.0.65\\lib\\josso-users.xml"));
				
				//2) Modificamos el archivo \lib\josso-credentials.xml
				File xmlFile2 = new File("C:\\apache-tomcat-7.0.65\\lib\\josso-credentials.xml");
				
				Document document2 = (Document)builder.build(xmlFile2);
				Element rootNode2 = document2.getRootElement();
				
				//Creo una nueva etiqueta <credential-set>
				Element credentialSet = new Element("credential-set");
				
				//Etiqueta <key>
				Element key = new Element("key");
				key.setText(uName);
				
				Element credential1 = new Element("credential");
				Element name1Credential1 = new Element("name");
				Element value1Credential1 = new Element("value");
				
				name1Credential1.setText("username");
				value1Credential1.setText(uName);
				credential1.addContent(name1Credential1);
				credential1.addContent(value1Credential1);
				
				Element credential2 = new Element("credential");
				Element name2Credential2 = new Element("name");
				Element value2Credential2 = new Element("value");
				
				name2Credential2.setText("password");
				//Conversion a MD5
				/*byte [] bytesOfMessage = password.getBytes("UTF-8");
				MessageDigest md;
				try {
					md = MessageDigest.getInstance("MD5");
					byte [] theDigest = md.digest(bytesOfMessage);
					String digerido = theDigest.toString();
					value2Credential2.setText(digerido);
				} catch (NoSuchAlgorithmException e1) {
					e1.printStackTrace();
				}*/
				
				credential2.addContent(name2Credential2);
				credential2.addContent(value2Credential2);
				
				credentialSet.addContent(key); //username
				credentialSet.addContent(credential1); //username
				credentialSet.addContent(credential2); //password
				
				rootNode2.addContent(credentialSet);
				
				Format forma2 = Format.getPrettyFormat();
				XMLOutputter xmloutputter2 = new XMLOutputter(forma2);
				xmloutputter2.output(document2,new FileWriter("C:\\apache-tomcat-7.0.65\\lib\\josso-credentials.xml"));	
			
				RequestDispatcher rd = request.getRequestDispatcher("C:\\apache-tomcat-7.0.65\\webapps\\josso\\signon\\usernamePasswordLogin.jsp");
				out.print("<font color = green> Usuario registrado correctamente</font>");
				rd.include(request, response);
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
