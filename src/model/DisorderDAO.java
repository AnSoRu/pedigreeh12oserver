package model;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import entities.Disorder;

public class DisorderDAO {

	static final Logger icdLogger = LogManager.getLogger(DisorderDAO.class.getName());

	public List<Disorder> findAllDisorderCodes(){
		icdLogger.info("Entering findAllDisorderCodes.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Disorder> list = (List<Disorder>)sess.createCriteria(Disorder.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		icdLogger.info("Exiting findAllDisorderCodes.");
		return list;		
	}

	public boolean insertDisorderCode(Disorder disorderCode){
		icdLogger.info("Entering insertDisorderCode.");
		icdLogger.info("Trying to insert disorder code with ID = " + disorderCode.getDisorderCode());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Disorder> aux = sess.createCriteria(Disorder.class).add(Restrictions.idEq(disorderCode.getDisorderCode())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			icdLogger.error("The disorder code with ID = " + disorderCode.getDisorderCode() + " already exists.");
			icdLogger.info("Exiting insertDisorderCode.");
			return false;
		}
		disorderCode.setLastModification(new Date());
		sess.save(disorderCode);
		sess.getTransaction().commit();
		sess.close();
		icdLogger.info("Disorder code with ID = " + disorderCode.getDisorderCode() + " inserted correctly.");
		icdLogger.info("Exiting insertDisorderCode.");
		return true;		
	}

	public List<Disorder> getDisorderCodeCriteria(Disorder disorderCode){
		icdLogger.info("Entering getDisorderCodeCriteria.");
		icdLogger.info("Trying to obtain disorder with ID = " + disorderCode);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Disorder> l = sess.createCriteria(Disorder.class).add(Restrictions.idEq(disorderCode.getDisorderCode())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			icdLogger.info("Unable to obtain disorder code with ID = " + disorderCode.getDisorderCode() + " because doesn't exist.");			
		}
		icdLogger.info("Exiting getDisorderCodeCriteria.");
		return l;
	}

	public void updateDisorderCode(Disorder disorderCode, Date d){
		icdLogger.info("Entering updateDisorderCode.");
		icdLogger.info("Trying to obtain disorder code with ID = " + disorderCode.getDisorderCode());
		List<Disorder> lAux = getDisorderCodeCriteria(disorderCode);
		if(!lAux.isEmpty()){
			icdLogger.info("Trying to obtain the last modification date of disorder with ID = " + disorderCode.getDisorderCode());
			Disorder dAux = lAux.get(0);
			Date dUltima = dAux.getLastModification();
			icdLogger.info("Last modification was on " + dUltima.toString());
			if(dUltima.compareTo(d)<0){
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				disorderCode.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(disorderCode);
				sess.getTransaction().commit();
				sess.close();
				icdLogger.info("Disorder code with ID = " + disorderCode.getDisorderCode() + " updated correctly.");
			}
		}
		icdLogger.info("Exiting updateDisorderCode.");
	}

	public boolean deleteIcdCodeByID(String disorderID){
		icdLogger.info("Entering deleteDisorderCodeByID.");
		icdLogger.info("Trying to delete disorder with ID = " + disorderID);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Disorder> l = sess.createCriteria(Disorder.class).add(Restrictions.idEq(disorderID)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.beginTransaction();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			sess.close();
			icdLogger.info("Disorder with ID = " + disorderID + " deleted correctly.");
		}
		else{
			res = false;
			icdLogger.info("Unable to delete disorder code with ID = " + disorderID + " because doesn't exist.");
		}
		icdLogger.info("Exiting deleteDisorderCodeByID.");
		return res;
	}
}
