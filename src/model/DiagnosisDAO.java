package model;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import entities.Diagnosis;

public class DiagnosisDAO {
	static final Logger dLogger = LogManager.getLogger(DiagnosisDAO.class.getName());

	public List<Diagnosis> findAllDiagnosises(){
		dLogger.info("Entering findAllDiagnosises.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Diagnosis> list = (List<Diagnosis>)sess.createCriteria(Diagnosis.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("idDiagnosis")).list();
		sess.close();
		dLogger.info("Exiting findAllDiagnosises.");
		return list;
	}

	public boolean insertDiagnosis(Diagnosis d){
		dLogger.info("Entering insertDiagnosis.");
		dLogger.info("Trying to insert diagnosis with ID = " + d.getIdDiagnosis());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Diagnosis> aux = sess.createCriteria(Diagnosis.class).add(Restrictions.idEq(d.getIdDiagnosis())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			dLogger.error("The diagnosis with ID = " + d.getIdDiagnosis() + " already exists.");
			dLogger.info("Exiting insertDiagnosis.");
			return false;
		}
		d.setLastModification(new Date());
		sess.save(d);
		sess.getTransaction().commit();
		sess.close();
		dLogger.info("Diagnosis with ID = " + d.getIdDiagnosis() + " inserted correctly.");
		dLogger.info("Exiting insertDiagnosis.");
		return true;
	}

	public List<Diagnosis> getDiagnosisCriteria(int diagnosisId){
		dLogger.info("Entering getDiagnosisCriteria.");
		dLogger.info("Trying to obtain diagnosis with ID = " + diagnosisId);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Diagnosis> l = sess.createCriteria(Diagnosis.class).add(Restrictions.idEq(diagnosisId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			dLogger.info("Unable to obtain diagnosis with ID = " + diagnosisId + " because doesn't exist.");
		}
		dLogger.info("Exiting getDiagnosisCriteria.");
		return l;		
	}

	public void updateDiagnosis(Diagnosis d, Date da){
		dLogger.info("Entering updateDiagnosis.");
		dLogger.info("Trying to update diagnosis with ID = " + d.getIdDiagnosis());
		List<Diagnosis> lAux = getDiagnosisCriteria(d.getIdDiagnosis());
		if(!lAux.isEmpty()){
			dLogger.info("Trying to obtain the last modification date of diagnosis with ID = " + d.getIdDiagnosis());
			Diagnosis dAux = lAux.get(0);
			Date dUltima = dAux.getDateofdiagnosis();
			dLogger.info("Last modification was on " + dUltima.toString());
			if(dUltima.compareTo(da)<0){
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				d.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(d);
				sess.getTransaction().commit();
				sess.close();
				dLogger.info("Diagnosis with ID = " + d.getIdDiagnosis() + " updated correctly.");
			}
		}
		dLogger.info("Exiting updateDiagnosis.");		
	}

	public boolean deleteDiagnosisByID(int diagnosisId){
		dLogger.info("Entering deleteDiagnosisByID.");
		dLogger.info("Trying to delete diagnosis with ID = " + diagnosisId);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Diagnosis> l = sess.createCriteria(Diagnosis.class).add(Restrictions.idEq(diagnosisId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.beginTransaction();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			sess.close();
			dLogger.info("Diagnosis with ID = " + diagnosisId + " deleted correctly.");
		}
		else{
			res = false;
			dLogger.info("Unable to delete person with ID = " + diagnosisId + " because doesn't exist.");
		}
		dLogger.info("Exiting deleteDiagnosisByID.");
		return res;
	}
}
