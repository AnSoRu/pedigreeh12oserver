package model;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import entities.Person;

public class PersonDAO {

	static final Logger pLogger = LogManager.getLogger(PersonDAO.class.getName());


	public List<Person> findAllPersons(){
		pLogger.info("Entering findAllPersons.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Person> list = (List<Person>)sess.createCriteria(Person.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("clinicalhistoryid")).list();
		sess.close();
		pLogger.info("Exiting findAllPersons.");
		return list;
	}

	public boolean insertPerson(Person p){
		pLogger.info("Entering insertPerson.");
		pLogger.info("Trying to insert person with ID = " + p.getClinicalhistoryid());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Person> aux = sess.createCriteria(Person.class).add(Restrictions.idEq(p.getClinicalhistoryid())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			pLogger.error("The person with ID = " + p.getClinicalhistoryid() + " already exists.");
			pLogger.info("Exiting insertPerson.");
			return false;
		}
		p.setLastModification(new Date());
		sess.save(p);
		sess.getTransaction().commit();
		sess.close();
		pLogger.info("Person with ID = " + p.getClinicalhistoryid() + " inserted correctly.");
		pLogger.info("Exiting insertPerson.");
		return true;
	}

	public List<Person> getPersonCriteria(String personId){
		pLogger.info("Entering getPersonCriteria.");
		pLogger.info("Trying to obtain person with ID = " + personId);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Person> l = sess.createCriteria(Person.class).add(Restrictions.idEq(personId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			pLogger.info("Unable to obtain person with ID = " + personId + " because doesn't exist.");
		}
		pLogger.info("Exiting getPersonCriteria.");
		return l;
	}

	public void updatePerson(Person p, Date d){
		pLogger.info("Entering updatePerson.");
		pLogger.info("Trying to update person with ID = " + p.getClinicalhistoryid());
		List<Person> lAux = getPersonCriteria(p.getClinicalhistoryid());
		if(!lAux.isEmpty()){
			pLogger.info("Trying to obtain the last modification date of person with ID = " + p.getClinicalhistoryid());
			Person pAux = lAux.get(0);
			Date dUltima = pAux.getLastModification();
			pLogger.info("Last modification was on " + dUltima.toString());
			if(dUltima.compareTo(d)<0){
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				p.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(p);
				sess.getTransaction().commit();
				sess.close();
				pLogger.info("Person with ID = " + p.getClinicalhistoryid() + " updated correctly.");
			}
		}
		pLogger.info("Exiting updatePerson.");
	}

	public boolean deletePersonByID(String personId){
		pLogger.info("Entering deletePersonByID.");
		pLogger.info("Trying to delete person with ID = " + personId);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Person> l = sess.createCriteria(Person.class).add(Restrictions.idEq(personId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.beginTransaction();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			sess.close();
			pLogger.info("Person with ID = " + personId + " deleted correctly.");
		}
		else{
			res = false;
			pLogger.error("Unable to delete person with ID = " + personId + " because doesn't exist.");
		}
		pLogger.info("Exiting deletePersonByID.");
		return res;
	}
}
