package model;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import entities.Birth;

public class BirthDAO {

	static final Logger bLogger = LogManager.getLogger(BirthDAO.class.getName());

	public List<Birth> findAllBirths(){
		bLogger.info("Entering findAllBirths.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Birth> list = (List<Birth>)sess.createCriteria(Birth.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("birthid")).list();
		sess.close();
		bLogger.info("Exiting findAllBirths.");
		return list;
	}

	public boolean insertBirth(Birth b){
		bLogger.info("Entering insertBirth.");
		bLogger.info("Trying to insert birth with ID = " + b.getBirthid());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Birth> aux = sess.createCriteria(Birth.class).add(Restrictions.idEq(b.getBirthid())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			bLogger.error("The birth with ID = " + b.getBirthid() + " already exists.");
			bLogger.info("Exiting insertBirth.");
			return false;
		}
		b.setLastModification(new Date());
		sess.save(b);
		sess.getTransaction().commit();
		sess.close();
		bLogger.info("Birth with ID = " + b.getBirthid() + " inserted correctly.");
		bLogger.info("Exiting insertBirth.");
		return true;
	}

	public List<Birth> getBirthCriteria(int birthId){
		bLogger.info("Entering getBirthCriteria.");
		bLogger.info("Trying to obtain birth with ID = " + birthId);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Birth> l = sess.createCriteria(Birth.class).add(Restrictions.idEq(birthId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			bLogger.info("Unable to obtain birth with ID = " + birthId + " because doesn't exist.");
		}
		bLogger.info("Exiting getBirthCriteria.");
		return l;
	}

	//M�todo especial
	//Consultar la fecha de �ltima modificaci�n
	public void updateBirth(Birth b, Date d){
		bLogger.info("Entering updateBirth.");
		bLogger.info("Trying to update birth with ID = " + b.getBirthid());
		List<Birth> lAux = getBirthCriteria(b.getBirthid());
		if(!lAux.isEmpty()){
			//Obtener la fecha de �ltima modificaci�n
			bLogger.info("Trying to obtain the last modification date of birth with ID = " + b.getBirthid());
			Birth bAux = lAux.get(0);
			Date dUltima = bAux.getLastModification();
			bLogger.info("Last modification was on " + dUltima.toString());
			//Caso 1: la fecha almacenada es mucho m�s anterior > 1 seg -> Actualizar
			if(dUltima.compareTo(d)<0){ //Significa que la fecha almacenada es ANTERIOR a la que le paso
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				b.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(b);
				sess.getTransaction().commit();
				sess.close();
				bLogger.info("Birth with ID = " + b.getBirthid() + " updated correctly.");
			}
		}
		bLogger.info("Exiting updateBirth.");
	}

	public boolean deleteBirthByID(int birthId){
		bLogger.info("Entering deleteBirthByID.");
		bLogger.info("Trying to delete birth with ID = " + birthId);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Birth> l = sess.createCriteria(Birth.class).add(Restrictions.idEq(birthId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			sess.close();
			bLogger.info("Birth with ID = " + birthId + " deleted correctly.");
		}
		else{
			res = false;
			bLogger.error("Unable to delete person with ID = " + birthId + " because doesn't exist.");
		}
		bLogger.info("Exiting deleteBirthByID.");
		return res;
	}	
}
