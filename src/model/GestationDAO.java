package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import entities.Gestation;
import entities.Union;

public class GestationDAO {

	static final Logger gLogger = LogManager.getLogger(GestationDAO.class.getName());

	public List<Gestation> findAllGestations(){
		gLogger.info("Entering findAllGestations.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Gestation> list = (List<Gestation>)sess.createCriteria(Gestation.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("idGestation")).list();
		sess.close();
		gLogger.info("Exiting findAllGestations.");
		return list;
	}

	public boolean insertGestation(Gestation g){
		gLogger.info("Entering insertGestation.");
		gLogger.info("Trying to insert gestation with ID = " + g.getIdGestation());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Gestation> aux = sess.createCriteria(Gestation.class).add(Restrictions.idEq(g.getIdGestation())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			gLogger.error("The gestation with ID = " + g.getIdGestation() + " already exists.");
			gLogger.info("Exiting insertGestation.");
			return false;
		}
		g.setLastModification(new Date());
		sess.save(g);
		sess.getTransaction().commit();
		sess.close();
		gLogger.info("Gestation with ID = " + g.getIdGestation() + " inserted correctly.");
		gLogger.info("Exiting insertGestation.");
		return true;
	}

	public List<Gestation> getGestationCriteria(int gestationId){
		gLogger.info("Entering getGestationCriteria.");
		gLogger.info("Trying to obtain gestation with ID = " + gestationId);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Gestation> l = sess.createCriteria(Gestation.class).add(Restrictions.idEq(gestationId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			gLogger.info("Unable to obtain gestation with ID = " + gestationId + " because doesn't exist.");
		}
		gLogger.info("Exiting getGestationCriteria.");
		return l;
	}

	public List<Gestation> getGestationsFromUnion(Union u){
		gLogger.info("Entering getGestationFromUnion.");
		gLogger.info("Trying to obtain all gestations from union with ID = " + u.getUnionId());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		//Query q = sess.createSQLQuery("select * from bbddGestation where gestation.Union_unionId=" + u.getUnionId()).addEntity(Gestation.class);
		@SuppressWarnings("unchecked")
		//List<Gestation> ls = q.list();
		List<Union> lAux = sess.createCriteria(Union.class).add(Restrictions.idEq(u.getUnionId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		List<Gestation> ls = new ArrayList<Gestation>();
		if(!lAux.isEmpty()){
			//Union uAux = lAux.get(0);
			sess = sf.openSession();
			sess.beginTransaction();
			System.out.println(u.getUnionId());
			@SuppressWarnings("unchecked")
			//List<Gestation> lGest = sess.createCriteria(Gestation.class).add(Restrictions.sqlRestriction("{alias} = select * from bbddclinica.Gestation where bbddclinica.Gestation.Union_unionId=" + u.getUnionId())).list();
			List<Gestation> lGest = sess.createSQLQuery("SELECT * FROM bbddclinica.Gestation WHERE bbddclinica.Gestation.Union_unionId=" + u.getUnionId()).addEntity(Gestation.class).list();
			//.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("idGestation")).list();
			//Set<Gestation> gAux = uAux.getGestations();
			sess.close();
			if(!lGest.isEmpty()){
				for (Gestation g : lGest){
					ls.add(g);
				}
			}
		}
		gLogger.info("Exiting getGestationsFromUnion.");
		return ls;
	}

	public void updateGestation(Gestation g, Date d){
		gLogger.info("Entering updateGestation.");
		gLogger.info("Trying to update gestation with ID = " + g.getIdGestation());
		List<Gestation> lAux = getGestationCriteria(g.getIdGestation());
		if(!lAux.isEmpty()){
			gLogger.info("Trying to obtain the last modification date of birth with ID = " + g.getIdGestation());
			Gestation gAux = lAux.get(0);
			Date dUltima = gAux.getLastModification();
			gLogger.info("Last modification was on " + dUltima.toString());
			if(dUltima.compareTo(d)<0){
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				g.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(g);
				sess.getTransaction().commit();
				sess.close();
				gLogger.info("Gestation with ID = " + g.getIdGestation() + " updated correctly.");
			}
		}
		gLogger.info("Exiting updateGestation.");
	}

	public boolean deleteGestationByID(int gestationId){
		gLogger.info("Entering deleteGestationByID.");
		gLogger.info("Trying to delete gestation with ID = " + gestationId);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Gestation> l = sess.createCriteria(Gestation.class).add(Restrictions.idEq(gestationId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.beginTransaction();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			gLogger.info("Gestation with ID = " + gestationId + " deleted correctly.");
		}
		else{
			res = false;
			gLogger.error("Unable to delete gestation with ID = " + gestationId + " because doesn't exist.");
		}
		gLogger.info("Exiting deleteGestationByID.");
		return res;
	}	
}
