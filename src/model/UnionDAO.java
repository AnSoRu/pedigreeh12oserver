package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import entities.Person;
import entities.Union;

public class UnionDAO {

	static final Logger uLogger = LogManager.getLogger(UnionDAO.class.getName());

	public List<Union> findAllUnions(){
		uLogger.info("Entering findAllUnions.");
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Union> list = (List<Union>)sess.createCriteria(Union.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.asc("unionId")).list();
		sess.close();
		uLogger.info("Exiting findAllUnions.");
		return list;
	}

	public List<Union> findAllUnionsForPersonByID(String personId){
		uLogger.info("Entering findAllUnionsForPersonByID.");
		uLogger.info("Trying to obtain all unions for person with ID = " + personId);
		List<Union> res = new ArrayList<Union>();
		PersonDAO pd = new PersonDAO();
		List<Person> personRes = pd.getPersonCriteria(personId);
		if(!personRes.isEmpty()){
			Person pAux = personRes.get(0);
			Set<Union> aux = pAux.getUnions();
			for(Union u : aux){
				res.add(u);
			}
		}
		return res;
	}

	public boolean insertUnion(Union u){
		uLogger.info("Entering insertUnion.");
		uLogger.info("Trying to insert union with ID = " + u.getUnionId());
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		sess.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Union> aux = sess.createCriteria(Union.class).add(Restrictions.idEq(u.getUnionId())).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		if(!aux.isEmpty()){
			sess.close();
			uLogger.info("The union with ID = " + u.getUnionId() + " already exists.");
			uLogger.info("Exiting insertUnion.");
			return false;
		}
		u.setLastModification(new Date());
		sess.save(u);
		sess.getTransaction().commit();
		sess.close();
		uLogger.info("Union with ID = " + u.getUnionId() + " inserted correctly.");
		uLogger.info("Exiting insertUnion.");
		return true;
	}

	public List<Union> getUnionCriteria(int unionId){
		uLogger.info("Entering getUnionCriteria.");
		uLogger.info("Trying to obtain union with ID = " + unionId);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Union> l = sess.createCriteria(Union.class).add(Restrictions.idEq(unionId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(l.isEmpty()){
			uLogger.error("Unable to obtain union with ID = " + unionId + " because doesn't exist." );
		}
		uLogger.info("Exiting getUnionCriteria.");
		return l;
	}

	public List<Union> getUnionForPeople(String p1 , String p2){
		uLogger.info("Entering getUnionForPeople.");
		List<Union> uRes = new ArrayList<Union>();
		@SuppressWarnings("unused")
		boolean unidas = false;
		uLogger.info("Trying to get person with ID = " + p1);
		PersonDAO pd = new PersonDAO();			
		List<Person> p1AuxL = pd.getPersonCriteria(p1);
		List<Person> p2AuxL = pd.getPersonCriteria(p2);		
		if(!p1AuxL.isEmpty() && !p2AuxL.isEmpty()){
			Person p1Aux = p1AuxL.get(0);
			Person p2Aux = p2AuxL.get(0);
			Set<Union> unionesP1 = p1Aux.getUnions();
			Set<Union> unionesP2 = p2Aux.getUnions();
			for(Union u1: unionesP1){
				for(Union u2 : unionesP2){
					if(u2.getUnionId() == u1.getUnionId()){
						unidas = true;
						uRes.add(u2);
						break;
					}
				}
				if(unidas = true){
					break;
				}
			}
		}
		else{
			if(p1AuxL.isEmpty()){
				uLogger.error("Unable to get person with ID = " + p1 + " because doesn't exist.");
			}
			if(p2AuxL.isEmpty()){
				uLogger.error("Unable to get person with ID = " + p2 + " because doesn't exist.");
			}
		}
		uLogger.info("Exiting getUnionForPeople.");
		return uRes;		
	}

	public void updateUnion(Union u, Date d){
		uLogger.info("Entering updateUnion.");
		uLogger.info("Trying to update person with ID = " + u.getUnionId());
		List<Union> lAux = getUnionCriteria(u.getUnionId());
		if(!lAux.isEmpty()){
			uLogger.info("Trying to obtain the last modification date of union with ID = " + u.getUnionId());
			Union uAux = lAux.get(0);
			Date dUltima = uAux.getLastModification();
			uLogger.info("Last modification was on " + dUltima.toString());
			if(dUltima.compareTo(d)<0){
				SessionFactory sf = HibernateUtil.getSessionFactory();
				Session sess = sf.openSession();
				u.setLastModification(new Date());
				sess.beginTransaction();
				sess.update(u);
				sess.getTransaction().commit();
				sess.close();
				uLogger.info("Union with ID = " + u.getUnionId() + " updated correctly.");
			}
		}
		uLogger.info("Exiting updateUnion.");
	}

	public boolean deleteUnionByID(int unionId){
		uLogger.info("Entering deleteUnionByID.");
		uLogger.info("Trying to delete union with ID = " + unionId);
		boolean res = false;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session sess = sf.openSession();
		@SuppressWarnings("unchecked")
		List<Union> l = sess.createCriteria(Union.class).add(Restrictions.idEq(unionId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		sess.close();
		if(!l.isEmpty()){
			res = true;
			sess = sf.openSession();
			sess.beginTransaction();
			sess.delete(l.get(0));
			sess.getTransaction().commit();
			sess.close();
			uLogger.info("Union with ID = " + unionId + " deleted correctly.");
		}
		else{
			res = false;
			uLogger.error("Unable to delete union with ID = " + unionId + " because doesn't exist.");
		}
		uLogger.info("Exiting deletePersonByID.");
		return res;
	}	
}
